export function uploadObject(param) {
    return new Promise((resolve, reject) => {
        wx.uploadFile({
            url: 'https://appdine.fairycs.club/common/upload',
            // url: 'https://devdine.fairycs.club/common/upload',
            // url: 'http://192.168.0.112:8080/common/upload',
            filePath: param.url,
            name: 'file',
            formData: {
                type: param.type
            },
         
            success: (res) => {
                resolve(JSON.parse(res.data))
            },
            fail: (res) => {
                reject(JSON.parse(res.data))
            }
        });
    })
}