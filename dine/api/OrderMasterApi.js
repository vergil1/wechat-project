import {request} from "@utils/request"
const prefix="/order-master"

export function getPageList(queryParam){
  return request({
    url: prefix+"/getPageList",
    method:"POST",
    data:queryParam
  })
}

export function getOrderDetailByOrderId(orderId){
  return request({
    url: prefix+"/getOrderDetailByOrderId?orderId="+orderId,
    method:"GET"
  })
}

export function saveOrder(param){
  return request({
    url: prefix+"/saveOrder",
    method:"POST",
    data:param
  })
}

export function updateOrder(param){
  return request({
    url: prefix+"/updateOrder",
    method:"PUT",
    data:param
  })
}

export function getPageListByAdmin(queryParam){
  return request({
    url: prefix+"/getPageListByAdmin",
    method:"POST",
    data:queryParam
  })
}

export function getOrderTotalWithTypeByUserId(userId){
  return request({
    url: prefix+"/getOrderTotalWithTypeByUserId?userId="+userId,
    method:"GET",
  })
}
export function getOrderDetailByOrderIds(ids){
  return request({
    url: prefix+"/getOrderDetailByOrderIds",
    method:"POST",
    data:ids
  })
}
/*管理员获取需要处理订单数量 */
export function getDealCount(){
  return request({
    url: prefix+"/getDealCount",
    method:"GET"
  })
}
/*提交订单 */
export function orderCommit(orderList){
  return request({
    url: prefix+"/orderCommit",
    method:"POST",
    data:orderList
  })
}
/*提醒发货 */
export function reminderForShipment(orderMaster){
  return request({
    url: prefix+"/reminderForShipment",
    method:"POST",
    data:orderMaster
  })
}
/**
 * 再次添加购物车
 * @param {*} orderId 
 */
export function addOrderCartAgain(orderId){
  return request({
    url: prefix+"/addOrderCartAgain?orderId="+orderId,
    method:"GET"
  })
}

/**
 * 堂食提交订单
 * @param {*} orderId 
 */
export function dineOrderCommit(param){
  return request({
    url: prefix+"/dineOrderCommit",
    method:"POST",
    data:param
  })
}

/**
 * 堂食历史账单
 * @param {*} orderId 
 */
export function queryBillByDate(param){
  return request({
    url: prefix+"/queryBillByDate",
    method:"POST",
    data:param
  })
}

/**
 * 堂食取消订单
 * @param {*} orderId 
 */
export function dineOrderCancel(params){
  return request({
    url: prefix+"/dineOrderCancel",
    method:"PUT",
    data:params
  })
}


/**
 * 今日账单
 * @param {*} orderId 
 */
export function queryTotalBIll(){
  return request({
    url: prefix+"/queryTotalBIll",
    method:"POST",
  })
}


