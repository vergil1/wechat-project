import {request} from "@utils/request"
const prefix="/message"
/**
 * 获取未读信息
 */
export function getUnReadTotal(){
  return request({
    url: prefix+"/getUnReadTotal",
    method:"GET"
  })
}
/**
 * 分页查询消息
 * @param {*} param 
 */
export function getPageListByParam(param){
  return request({
    url: prefix+"/getPageListByParam",
    method:"POST",
    data:param
  })
}
/**
 * 详情并转已读
 * @param {*} id 
 */
export function getDetailById(id){
  return request({
    url: prefix+"?id="+id,
    method:"GET"
  })
}


