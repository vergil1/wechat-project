import {request} from "../utils/request"
const prefix="/sku-type"

export function getAll(){
  return request({
    url: prefix,
    method:"POST"
  })
}


export function getTypeById(id){
  return request({
    url: prefix+"?id="+id,
    method:"GET"
  })
}

export function add(param){
  return request({
    url: prefix+"/add",
    method:"POST",
    data:param
  })
}
export function modify(param){
  return request({
    url: prefix+"/modify",
    method:"PUT",
    data:param
  })
}
export function deleteById(id){
  return request({
    url: prefix+"?id="+id,
    method:"DELETE"
  })
}


export function queryTypeClassByProductId(productId){
  return request({
    url: prefix+"/queryTypeClassByProductId"+"?productId="+productId,
    method:"GET"
  })
}