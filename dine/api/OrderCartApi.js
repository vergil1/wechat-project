import {request} from "../utils/request"
const prefix="/order-cart"

export function getOrderCartsByUserId(userId,type){
  return request({
    url: prefix+"/getOrderCartsByUserId?userId="+userId+"&type="+type,
    method:"GET"
  })
}

export function addSkuToCart(param){
  return request({
    url: prefix+"/addSkuToCart",
    method:"POST",
    data:param
  })
}
export function modifySkuAmount(param){
  return request({
    url: prefix+"/modifySkuAmount",
    method:"PUT",
    data:param
  })
}
export function deleteOrderCartByItemId(id){
  return request({
    url: prefix+"/deleteOrderCartByItemId"+"?id="+id,
    method:"DELETE"
  })
}

export function getCategoryByCategoryId(id){
  return request({
    url: prefix+"/getCategoryByCategoryId"+"?id="+id,
    method:"GET"
  })
}

export function commitOrderInCart(orderCarts){
  return request({
    url: prefix+"/commitOrderInCart",
    method:"POST",
    data:orderCarts
  })
}
/**
 * 清空购物车
 * @param {*} type 
 */
export function clearOrderCart(type){
  return request({
    url: prefix+"/clearOrderCart"+"?type="+type,
    method:"DELETE"
  })
}

export function getOrderCartsTotal(){
  return request({
    url: prefix+"/getOrderCartsTotal?type=2",
    method:"GET"
  })
}