import {request} from "../utils/request"
const prefix="/product-comment"

export function addProductComment(param){
  return request({
    url: prefix+"/addProductComment",
    method:"POST",
    data:param
  })
}



export function getPageList(queryParam){
  return request({
    url: prefix+"/getPageList",
    method:"POST",
    data:queryParam
  })
}


