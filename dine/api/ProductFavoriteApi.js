import {request} from "../utils/request"
const prefix="/product-favorite"

export function getPageList(queryParam){
  return request({
    url: prefix+"/getPageList",
    method:"POST",
    data:queryParam,
  })
}
export function addFavoriteProduct(productId){
  return request({
    url: prefix+"/addFavoriteProduct?userId="+getApp().globalData.userInfo.userId+"&productId="+productId,
    method:"GET"
  })
}
export function deleteFavoriteProduct(productId){
  return request({
    url: prefix+"/deleteFavoriteProduct?userId="+getApp().globalData.userInfo.userId+"&productId="+productId,
    method:"DELETE"
  })
}
/**
 * return 获取商品是否目前用户收藏
 * @param {商品id} productId 
 */
export function getProductFavoriteByUserId(productId){
  return request({
    url: prefix+"?productId="+productId+"&userId="+getApp().globalData.userInfo.userId,
    method:"GET"
  })
}
