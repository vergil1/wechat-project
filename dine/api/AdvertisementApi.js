import {request} from "../utils/request"
const prefix="/advertisement"

export function getAdvertisement(){
  return request({
    url: prefix,
    method:"POST"
  })
}
/**
 * 分页查询V1
 * @param {查询条件} queryParam 
 */
export function getPageList(queryParam){
  return request({
    url: prefix+"/getPageList",
    method:"POST",
    data:queryParam
  })
}
/**
 * 分页查询V2
 * @param {查询条件} queryParam 
 */
export function getPageListByParam(queryParam){
  return request({
    url: prefix+"/getPageListByParam",
    method:"POST",
    data:queryParam
  })
}
export function addAdvertisement(param){
  return request({
    url: prefix+"/addAdvertisement",
    method:"POST",
    data:param
  })
}

export function modifyAdvertisement(param){
  return request({
    url: prefix+"/modifyAdvertisement",
    method:"PUT",
    data:param
  })
}

export function deleteAdvertisement(param){
  return request({
    url: prefix+"?id="+param,
    method:"DELETE"
  })
}


