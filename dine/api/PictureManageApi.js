import {request} from "@utils/request"
const prefix="/picture-manage"
export function getPageList(queryParam){
  return request({
    url: prefix+"/getPageList",
    method:"POST",
    data:queryParam
  })
}
//图片管理-查询
export function getList(){
  return request({
    url: prefix+"/getList",
    method:"GET",
  })
}
export function getById(pictureId){
  return request({
    url: prefix+"/getById/"+pictureId,
    method:"GET",
  })
}
export function savePicture(param){
  return request({
    url: prefix+"/savePicture",
    method:"POST",
    data:param
  })
}

export function updatePicture(param){
  return request({
    url: prefix+"/updatePicture",
    method:"PUT",
    data:param
  })
}

//订单-获取物流图片
export function getLogisticsPicUrl(userId,orderSn){
  return request({
    url: prefix+"/getLogisticsPicUrl/"+userId+"/"+orderSn,
    method:"GET",
  })
}
