import {request} from "@utils/request"
const prefix="/rooms"

export default function getRooms(){
  return request({
    url: prefix,
    method:"Get"
  })
}
