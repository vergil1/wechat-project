import {request} from '../utils/request'
const prefix="/product-info"

export function getPageList(param){
  return request({
    url: prefix+"/getPageList",
    method:"POST",
    data:param
  })
}

export function getDetailById(id){
  return request({
    url: prefix+"?id="+id,
    method:"GET",
  })
}

export function addProductInfo(param){
  return request({
    url: prefix+"/add",
    method:"POST",
    data:param
  })
}

export function modifyProductInfo(param){
  return request({
    url: prefix+"/modify",
    method:"PUT",
    data:param
  })
}

export function deleteProductInfo(id){
  return request({
    url: prefix+"?id="+id,
    method:"DELETE",
  })
}
//新榜 热榜
export function getIndexProduct(flag){
  return request({
    url: prefix+"/getIndexProduct?flag="+flag,
    method:"GET",
  })
}
//查看商品详情
export function getProductDetailByProductId(productId){
  return request({
    url: prefix+"/getProductDetailByProductId?productId="+productId,
    method:"GET",
  })
}

//搜索框搜索
export function getPageListBySearch(param){
  return request({
    url: prefix+"/getPageListBySearch",
    method:"POST",
    data:param
  })
}

//点击购物车/立即购买显示的sku规格
export function getProductAllSkuType(productId){
  return request({
    url: prefix+"/getProductAllSkuType?productId="+productId,
    method:"GET",
  })
}
