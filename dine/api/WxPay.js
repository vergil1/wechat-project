import {request} from "@utils/request"
const prefix="/wxPay"

export default function createOrder(params){
  return request({
    url: prefix+"/createOrder",
    method:"POST",
    data:params
  })
}
