import {request} from "@utils/request"
const prefix="/order-master-dine-detail"
/**
 * 后厨列表
 * @param {状态 0-未出餐 1-已出餐} state 
 */
export function getDishByChef(state){
  return request({
    url: prefix+"/getDishByChef/"+state,
    method:"GET"
  })
}
/**
 * 出餐
 * @param {*} state 
 */
export function ready(orderDetailId){
  return request({
    url: prefix+"/ready/"+orderDetailId,
    method:"PUT"
  })
}
