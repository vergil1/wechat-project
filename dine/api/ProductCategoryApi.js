import {request} from "@utils/request"
const prefix="/product-category"

export function getCategories(type){
  if (!type) {
    type=''
  }
  return request({
    url: prefix+"/getCategories?type="+type,
    method:"POST"
  })
}

export function addCategories(param){
  return request({
    url: prefix+"/add",
    method:"POST",
    data:param
  })
}
export function modifyCategories(param){
  return request({
    url: prefix+"/modify",
    method:"PUT",
    data:param
  })
}
export function deleteCategories(id){
  return request({
    url: prefix+"/delete"+"?id="+id,
    method:"DELETE"
  })
}

export function getCategoryByCategoryId(id){
  return request({
    url: prefix+"/getCategoryByCategoryId"+"?id="+id,
    method:"GET"
  })
}
/**
 * 堂食列表
 */
export function getCategoryAndProductByDine(dineType){
  return request({
    url: prefix+"/getCategoryAndProductByDine/"+dineType,
    method:"GET"
  })
}