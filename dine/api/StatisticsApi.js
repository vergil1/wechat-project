import {request} from "@utils/request"
const prefix="/statistics"

export function getYearReport(year){
  return request({
    url: prefix+"/getYearReport/"+year,
    method:"GET"
  })
}

export function getMonthReport(year,month){
  return request({
    url: prefix+"/getMonthReport/"+year+"/"+month,
    method:"GET"
  })
}
export function getSalesReport(queryParam){
  return request({
    url: prefix+"/getSalesReport",
    method:"POST",
    data:queryParam
  })
}

export function getFavoriteReport(queryParam){
  return request({
    url: prefix+"/getFavoriteReport",
    method:"POST",
    data:queryParam
  })
}

/**
 * 餐饮
 */
/**
 * 销售菜品排行
 * @param {} queryParam 
 */
export function getDineSaleFoodRank(queryParam){
  return request({
    url: prefix+"/getDineSaleFoodRank",
    method:"POST",
    data:queryParam
  })
}
/**
 * 餐饮日报
 */
export function getEachDayTurnoverByMonth(date){
  return request({
    url: prefix+"/getEachDayTurnoverByMonth?date="+date,
    method:"GET",
  })
}
