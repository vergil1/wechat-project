import {request} from "../utils/request"
const prefix="/product-sku"
//获取所有分类
export function getCategories(){
  return request({
    url: prefix+"/getCategories",
    method:"POST"
  })
}
//详情页面
export function getDetailByProductId(skuId){
  return request({
    url: prefix+"/getDetailByProductId?skuId="+skuId,
    method:"Get"
  })
}
//列表
export function getListByProductId(productId){
  return request({
    url: prefix+"/getListByProductId?productId="+productId,
    method:"Get"
  })
}

export function addProductSku(param){
  return request({
    url: prefix+"/addProductSku",
    method:"POST",
    data:param
  })
}

export function modifyProductSku(param){
  return request({
    url: prefix+"/modifyProductSku",
    method:"PUT",
    data:param
  })
}

//根据skuValue查看商品sku
export function getProductSkuByTypeValueStr(param,productId){
  return request({
    url: prefix+"/getProductSkuByTypeValueStr?typeValueStr="+param+"&productId="+productId,
    method:"GET"
  })
}

//根据skuValue查看商品sku
export function getDineProductSkuByTypeValueStr(param,productId){
  return request({
    url: prefix+"/getDineProductSkuByTypeValueStr?typeValueStr="+param+"&productId="+productId,
    method:"GET"
  })
}