import {request} from "@utils/request"
const prefix="/order-master-dine"

export function getPageList(queryParam){
  return request({
    url: prefix+"/getPageList",
    method:"POST",
    data:queryParam
  })
}
/**
 * 堂食提交订单
 * @param {*} orderId 
 */
export function dineOrderCommit(param){
  return request({
    url: prefix+"/dineOrderCommit",
    method:"POST",
    data:param
  })
}

/**
 * 堂食历史账单
 * @param {*} orderId 
 */
export function queryBillByDate(param){
  return request({
    url: prefix+"/queryBillByDate",
    method:"POST",
    data:param
  })
}

/**
 * 堂食取消订单
 * @param {*} orderId 
 */
export function dineOrderCancel(params){
  return request({
    url: prefix+"/dineOrderCancel",
    method:"PUT",
    data:params
  })
}


/**
 * 今日账单
 * @param {*} orderId 
 */
export function queryTotalBIll(){
  return request({
    url: prefix+"/queryTotalBIll",
    method:"POST",
  })
}

/**
 * 订单详情
 * @param {*} orderId 
 */
export function detailByOrderSn(orderSn){
  return request({
    url: prefix+"/detailByOrderSn/"+orderSn,
    method:"GET",
  })
}

