import {request} from "../utils/request"
const prefix="/login"
//获取openId
export function getOpenId(code){
  return request({
    url: prefix+"/getOpenId?code="+code+"&appId="+getApp().globalData.appId,
    method:"GET"
  })
}

//获取手机号码并注册
export function getPhoneNumberAndRegister(code,openId){
  return request({
    url: prefix+"/getPhoneNumberAndRegister?code="+code+"&openId="+openId+"&appId="+getApp().globalData.appId,
    method:"POST"
  })
}

//获取手机号码并注册
export function updateUserInfo(userInfo){
  return request({
    url: prefix+"/updateUserInfo",
    method:"PUT",
    data:userInfo
  })
}

//自动登录
export function autoLogin(token){
  return request({
    url: prefix+"/autoLogin?token="+token,
    method:"GET"
  })
}









export function test(){
  return request({
    url: prefix+"/test",
    method:"GET"
  })
}
