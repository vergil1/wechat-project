import {request} from "../utils/request"
const prefix="/region"
//根据层级查当前区域
export function queryCurrentDivisionByDeep(deep){
  return request({
    url: prefix+"/queryCurrentDivisionByDeep?deep="+deep,
    method:"GET"
  })
}
//根据层级查当前区域
export function querySubordinateDivisionById(id){
  return request({
    url: prefix+"/querySubordinateDivisionById?id="+id,
    method:"GET"
  })
}

