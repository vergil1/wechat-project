import {request} from "@utils/request"
const prefix="/order-status-logs"
/**
 * 根据订单id查整个订单流程状态
 * @param {*} orderId 
 */
export function getProcessByOrderId(orderId){
  return request({
    url: prefix+"?orderId="+orderId,
    method:"GET"
  })
}
