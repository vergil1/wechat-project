import {
  request
} from "@utils/request"
const prefix = "/reservations"

export default function getReservationsByDate(date, roomId) {
  return request({
    url: prefix + "/getReservationsByDate?date=" + date + "&roomId=" + roomId,
    method: "GET"
  })
}
export function reserveRoom(param) {
  return request({
    url: prefix + "/reserveRoom",
    method: "POST",
    data: param
  })
}

export function dailyStatistics(date) {
  return request({
    url: prefix + "/dailyStatistics?date=" + date,
    method: "GET"
  })
}

export function getMyReservation() {
  return request({
    url: prefix + "/getMyReservation",
    method: "GET"
  })
}
/**
 * 支付失败后调用
 * @param {*} reservationId 
 */
export function roomOrderCancel(reservationId) {
  return request({
    url: prefix + "/roomOrderCancel/"+reservationId,
    method: "DELETE"
  })
}