import {request} from "@utils/request"
const prefix="/order-cart-dine"

export function getOrderCartsByUserId(){
  return request({
    url: prefix+"/getOrderCartsByUserId",
    method:"GET"
  })
}

export function addSkuToCart(param){
  return request({
    url: prefix+"/addSkuToCart",
    method:"POST",
    data:param
  })
}
export function modifySkuAmount(param){
  return request({
    url: prefix+"/modifySkuAmount",
    method:"PUT",
    data:param
  })
}
export function deleteOrderCartByItemId(id){
  return request({
    url: prefix+"/deleteOrderCartByItemId"+"?id="+id,
    method:"DELETE"
  })
}

/**
 * 清空购物车
 * @param {*} type 
 */
export function clearOrderCart(){
  return request({
    url: prefix+"/clearOrderCart",
    method:"DELETE"
  })
}

export function getOrderCartsTotal(){
  return request({
    url: prefix+"/getOrderCartsTotal",
    method:"GET"
  })
}