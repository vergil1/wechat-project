import {request} from '../utils/request'
const prefix="/product-pic-info"


export function getDetailById(id){
  return request({
    url: prefix+"?id="+id,
    method:"GET",
  })
}

export function getSingleById(id){
  return request({
    url: prefix+"/getSingleById?id="+id,
    method:"GET",
  })
}
export function addPicInfo(param){
  return request({
    url: prefix+"/add",
    method:"POST",
    data:param
  })
}
export function modifyPicInfo(param){
  return request({
    url: prefix+"/modify",
    method:"PUT",
    data:param
  })
}
export function deletePicInfoById(id){
  return request({
    url: prefix+"?id="+id,
    method:"DELETE"
  })
}