import {request} from "../utils/request"
const prefix="/customer-addr"

export function getAddressListByUserId(userId){
  return request({
    url: prefix+"/getAddressListByUserId"+"?userId="+userId,
    method:"GET"
  })
}

export function getAddressListByUserIdDefault(userId){
  return request({
    url: prefix+"/getAddressListByUserIdDefault"+"?userId="+userId,
    method:"GET"
  })
}

export function getAddressListByCustomerAddrId(customerAddrId){
  return request({
    url: prefix+"/getAddressListByCustomerAddrId"+"?customerAddrId="+customerAddrId,
    method:"GET"
  })
}

export function addCustomerAddress(param){
  return request({
    url: prefix+"/addCustomerAddress",
    method:"POST",
    data:param
  })
}
export function modifyCustomerAddress(param){
  return request({
    url: prefix+"/modifyCustomerAddress",
    method:"PUT",
    data:param
  })
}
export function deleteCustomerAddressById(id){
  return request({
    url: prefix+"/deleteCustomerAddressById?id="+id,
    method:"DELETE"
  })
}

