import {request} from "@utils/request"
const prefix="/dict"

export function dictByCondition(params){
  return request({
    url: prefix+"/dictByCondition",
    method:"POST",
    data:params
  })
}
