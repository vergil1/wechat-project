import {request} from "@utils/request"
const prefix="/user-info"

export function modifyUserInfo(param){
  return request({
    url: prefix+"/modifyUserInfo",
    method:"PUT",
    data:param
  })
}

export function getDetailInPersonPage(userId){
  return request({
    url: prefix+"/getDetailInPersonPage?userId="+userId,
    method:"GET",
  })
}


export function getUserByPhone(phone){
  return request({
    url: prefix+"/getUserByPhone?phone="+phone,
    method:"GET",
  })
}


