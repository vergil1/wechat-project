import {request} from "../utils/request"
const prefix="/sku-type-value"

export function addTypeValue(param){
  return request({
    url: prefix+"/add",
    method:"POST",
    data:param
  })
}
export function modifyTypeValue(param){
  return request({
    url: prefix+"/modify",
    method:"PUT",
    data:param
  })
}
export function deleteTypeValueById(id){
  return request({
    url: prefix+"?id="+id,
    method:"DELETE"
  })
}