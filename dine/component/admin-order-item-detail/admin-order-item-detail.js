// component/my-order-item-detail/my-order-item-detail.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    num: {
      type: Number,
      value: 0
    },
    singlePrice: {
      type: Number,
      value: 0
    },
    desc: {
      type: String,
      value: ''
    },
    productName: {
      type: String,
      value: ''
    },
    picUrl: {
      type: String,
      value: ''
    },
    productId:{
      type: Number,
      value: 0
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})