// component/img-item/img-item.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    src: {
      type: String,
      value: ''
    },
    //展开尺寸
    width:{
      type: Number,
      value: 0
    },
    height:{
      type: Number,
      value: 0
    },
    //浏览大小
    browseWidth:{
      type: Number,
      value: 0
    },
    browseHeight:{
      type: Number,
      value: 0
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    show: false,
    pdr: getApp().globalData.pdr,
    heightNum: getApp().globalData.heightNum,
    widthNum: getApp().globalData.widthNum,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onClickShow() {
      this.setData({ show: true });
    },

    onClickHide() {
      this.setData({
        show: false
      });
    },
    noop() {
      console.log(111);
    },

  }
})