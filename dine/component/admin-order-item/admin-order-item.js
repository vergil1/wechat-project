// component/my-order-item/my-order-item.js
import {updateOrder} from '../../api/OrderMasterApi'
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    num: {
      type: Number,
      value: 0
    },
    singlePrice: {
      type: Number,
      value: 0
    },
    desc: {
      type: String,
      value: ''
    },
    productName: {
      type: String,
      value: ''
    },
    picUrl: {
      type: String,
      value: ''
    },
    totalPrice: {
      type: Number,
      value: 0
    },
    orderStatus: {
      type: String,
      value: ''
    },
    orderId:{
      type: Number,
      value: 0
    },
    productId:{
      type: Number,
      value: 0
    },
    //当前页状态
    currPageStatus:{
      type: Number,
      value: 0
    },
    //物流单号
    shippingSn:{
      type: String,
      value: ''
    },
    shippingCompName:{
      type: String,
      value: ''
    },
  },

  

  /**
   * 组件的初始数据
   */
  data: {
    shippingDialogShow:false,
    //订单状态(0-待支付，1-待发货，2-待收货，3-待确认,5-待评价，6-退货审核，7-取消交易，8-已评价,10-取消审核，12-退货失败，15-退货未发货，17-退款已发货，20-退货已退款)
    statusList:["待支付","待发货","待收货","待确认","","待评价","退货审核","取消交易","已评价","","取消订单审核","","退货失败"
  ,"","","退货未发货","","退款已发货","","","退货已退款"]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //跳转评价页面
    onComment(e){
      console.log(e);
      wx.navigateTo({
        url: '/pages/me/myOrder/writeComment/writeComment?orderId='+e.currentTarget.dataset.orderid+'&productId='+e.currentTarget.dataset.productid,
      })
    },
    //跳转详情页面
    onToDetail(e){
      wx.navigateTo({
        url: '/pages/admin/orderAdmin/orderAdminDetail/orderAdminDetail?orderId='+e.currentTarget.dataset.orderid,
      })
    },
    //取消订单
    async cancelOrder(e){
      let {currstatus,orderid,currpagestatus} = e.currentTarget.dataset
      //待付款状态取消
      if (currstatus==0) {
        let {code,msg} = await updateOrder({orderId:orderid,orderStatus:7});
        if (code==200) {
          wx.showToast({
            title: '取消成功',
            icon:'none'
          })
          this.triggerEvent('rollbackTabRefresh',currpagestatus);
        }
        //待发货状态取消
      }else if(currstatus==1){
        let {code,msg} = await updateOrder({orderId:orderid,orderStatus:7});
        if (code==200) {
          wx.showToast({
            title: '取消成功',
            icon:'none'
          })
          this.triggerEvent('rollbackTabRefresh',currpagestatus);
        }
      }
    },
    //查看物流单号
    checkShippingSn(){
      // console.log(e);
      this.setData({
        shippingDialogShow:true
      })
    },
    //一键复制物流单号
    copyShippingSn(e){
      let {shippingsn} = e.currentTarget.dataset;
      wx.setClipboardData({
        data: shippingsn,
        success (res) {
          wx.showToast({
            title: '复制成功',
            icon:'none'
          })
        }
      })
    },
    //确认收货
    async confirmOrder(e){
      let {orderid,currpagestatus} = e.currentTarget.dataset
      let {code,msg} = await updateOrder({orderId:orderid,
        previousStatus:3,
        orderStatus:4});
        if (code==200) {
          wx.showToast({
            title: '确认收货成功',
            icon:'none'
          })
          this.triggerEvent('rollbackTabRefresh',currpagestatus);
        }
        
    }
  }
})