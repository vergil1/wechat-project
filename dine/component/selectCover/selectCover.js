// component/selectCover/selectCover.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    dis:{
      type:Boolean,
      value:true
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
  },

  /**
   * 组件的方法列表
   */
  methods: {
    food(){
      this.setData({
        dis:false
      })
      this.triggerEvent('init', 1);
    },
    drink(){
      this.setData({
        dis:false
      })
      this.triggerEvent('init', 2);
    }
  }
})