// component/my-order-item/my-order-item.js
import {
  updateOrder,
  reminderForShipment,
  addOrderCartAgain
} from '@api/OrderMasterApi'
import * as picApi from '@api/PictureManageApi'
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    num: {
      type: Number,
      value: 0
    },
    singlePrice: {
      type: Number,
      value: 0
    },
    desc: {
      type: String,
      value: ''
    },
    productName: {
      type: String,
      value: ''
    },
    picUrl: {
      type: String,
      value: ''
    },
    totalPrice: {
      type: Number,
      value: 0
    },
    orderStatus: {
      type: String,
      value: ''
    },
    orderId: {
      type: Number,
      value: 0
    },
    productId: {
      type: Number,
      value: 0
    },
    //当前页状态
    currPageStatus: {
      type: Number,
      value: 0
    },
    //物流单号
    shippingSn: {
      type: String,
      value: ''
    },
    shippingCompName: {
      type: String,
      value: ''
    },
    rejectReason: {
      type: String,
      value: ''
    },
    version: {
      type: Number,
      value: 0
    },
    cancelRejectReason: {
      type: String,
      value: ''
    },
    orderSn:{
      type: String,
      value: ''
    }
  },



  /**
   * 组件的初始数据
   */
  data: {
    cancelShowFlag: false,
    shippingDialogShow: false,
    auditFlag: false,
    cancelReason: [{
        name: '价格有点贵',
      },
      {
        name: '收货地址拍错',
      },
      {
        name: '规格/数量/款式拍错',
      },
      {
        name: '暂时不需要',
      },
      {
        name: '其他',
      },
    ],
    cancelOrderId: '', //取消订单号 暂存,
    currPageStatus: '', //当前状态页
    logisticsPicUrl:'',//物流消息
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //跳转评价页面
    onComment(e) {
      console.log(e);
      wx.navigateTo({
        url: '/pages/me/myOrder/writeComment/writeComment?orderId=' + e.currentTarget.dataset.orderid + '&productId=' + e.currentTarget.dataset.productid,
      })
    },
    //跳转详情页面
    onToDetail(e) {
      wx.navigateTo({
        url: '/pages/me/myOrder/detail/detail?orderId=' + e.currentTarget.dataset.orderid,
      })
    },

    //查看物流单号
    async checkShippingSn(e) {
      let {ordersn} = e.currentTarget.dataset;
      this.setData({
        shippingDialogShow: true
      })
      if(!this.data.logisticsPicUrl){
        let {data} = await picApi.getLogisticsPicUrl(getApp().globalData.userInfo.userId,ordersn)
        this.setData({
          logisticsPicUrl:data.picUrl
        })
      }
    },
    //一键复制物流单号
    copyShippingSn(e) {
      let {
        shippingsn
      } = e.currentTarget.dataset;
      wx.setClipboardData({
        data: shippingsn,
        success(res) {
          wx.showToast({
            title: '复制成功',
            icon: 'none'
          })
        }
      })
    },
    //确认收货
    async confirmOrder(e) {
      let {
        orderid,
        currpagestatus
      } = e.currentTarget.dataset
      let {
        code,
        msg
      } = await updateOrder({
        orderId: orderid,
        previousStatus:3,
        orderStatus: 5
      });
      if (code == 200) {
        wx.showToast({
          title: '确认收货成功',
          icon: 'none'
        })
        this.triggerEvent('rollbackTabRefresh', 98);
      }

    },
    //跳转退货页面
    toApplyForRefund(e) {
      let {orderid,version} = e.currentTarget.dataset
      wx.navigateTo({
        url: '/pages/me/myOrder/applyForRefund/applyForRefund?orderId=' + orderid+"&version="+version,
      })
    },
    toCheckRefund(e) {
      wx.navigateTo({
        url: '/pages/me/myOrder/checkRefund/checkRefund?orderId=' + e.currentTarget.dataset.orderid,
      })
    },
    //跳转取消订单申请页面
    toApplyForCancel(e) {
      let {orderid,version} = e.currentTarget.dataset
      wx.navigateTo({
        url: '/pages/me/myOrder/applyForCancel/applyForCancel?orderId=' + orderid+"&version="+version,
      })
    },
    //等待管理员审核
    checkRefundInAudit() {
      this.setData({
        auditFlag: true
      })
    },
    //立刻支付
    onPay(e) {
      let orderId = e.currentTarget.dataset.orderid
      wx.navigateTo({
        url: '/pages/settlement/settlement?ids=' + orderId,
      })
    },
    /**
     * 订单取消
     */
    //展示按钮
    cancelOrder(e) {
      let {
        orderid,
        version
      } = e.currentTarget.dataset
      this.setData({
        cancelOrderId: orderid,
        cancelShowFlag: true,
        currVersion: version,
      })
    },
    //取消按钮
    cancelReasonClose() {
      this.setData({
        cancelOrderId: '',
        cancelShowFlag: false,
        currVersion: 0
      })
    },
    //选中取消原因
    async selectCancelReason(e) {
      let cancelReason = e.detail.name
      let {
        cancelOrderId,
        currVersion
      } = this.data
      this.setData({
        cancelShowFlag: false
      })
      let {
        code,
        msg
      } = await updateOrder({
        orderId: cancelOrderId,
        orderStatus: 7,
        previousStatus: 0,
        cancelReason: cancelReason,
        version: currVersion
      })
      if (code == 200) {
        wx.showToast({
          title: '取消成功',
        })
        setTimeout(() => {
          this.triggerEvent('rollbackTabRefresh', this.properties.currPageStatus);
        }, 1000)
      } else {
        wx.showToast({
          title: msg,
          icon: 'error'
        })
      }
    },
    /**
     * 提醒发货
     */
    async onNotice(e) {
      let orderId = e.currentTarget.dataset.orderid
      let {
        msg
      } = await reminderForShipment({
        orderId: orderId
      })
      wx.showToast({
        title: msg,
        icon: "none"
      })
    },
    async addOrderCartAgain(e){
      let orderid = e.currentTarget.dataset.orderid
      let {code} = await addOrderCartAgain(orderid);
      if (code==200) {
        wx.showToast({
          title: '添加成功',
        })
      }
    },
    previewImage(){
      wx.previewImage({
        current: '', // 当前显示图片的http链接
        urls: [this.data.logisticsPicUrl] // 需要预览的图片http链接列表
      })
    }
    
  },
})