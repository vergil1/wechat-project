// component/my-button/my-button.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    color: {
      type: String,
      value: 'linear-gradient(to right,#f6c336,#fa5f26)' // 默认属性值
    },
    text: {
      type: String,
      value: '确定' // 默认属性值
    },
    show:{
      type:Boolean,
      value:false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})