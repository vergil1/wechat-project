// component/loading/loading.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    size:{type:Number,value:0}
  },

  /**
   * 组件的初始数据
   */
  data: {
    load:false
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})