// component/index-product-item/index-product-item.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
      product:{
        type:Array,
        value:[]
      }
  },

  /**
   * 组件的初始数据
   */
  data: {
 
  },

  /**
   * 组件的方法列表
   */
  methods: {
    toItemDetail(e){
      console.log(e);
      wx.navigateTo({
        url: '/pages/p_detail/p_detail?productId='+e.currentTarget.dataset.productid,
      })
    }
  },
  observers:{
    'product': function(val){
      // console.log('父组件传来：');
      // console.log(val)
    }
  }

})