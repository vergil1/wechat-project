Component({

  /**
   * 组件的属性列表
   */
  properties: {
    iUrl:{
      type:String,
      value:'',
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    time: 5, //5,
    realDisplay:true
  },
  lifetimes: {
    created() {
    },
    // 附和、附上
    attached() {
      if (this.data.realDisplay) {
        this.timer()
      }
    },
    // 离开
    detached() {
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    timer() {
      if (this.data.time >= 0) {
        setTimeout(() => {
          let time = this.data.time - 1
          this.setData({
            time
          })
          this.timer()
        }, 1000);
      } else {
        this.setData({
          realDisplay: false
        })
        getApp().globalData.display=false
        // wx.showTabBar()
        // this.triggerEvent('displaySearch', 'block');
      }
    },
    click1(){
      setTimeout(() => {
        this.setData({
          realDisplay:false
        })
        getApp().globalData.display=false
        // wx.showTabBar()
        // this.triggerEvent('displaySearch', 'block');
      }, 500);
    },
  }
})