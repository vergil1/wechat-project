export function timestampToTime(value, type = 0){
  var time = new Date(value);
  var year = time.getFullYear();
  var month = time.getMonth() + 1;
  var date = time.getDate();
  var hour = time.getHours();
  var minute = time.getMinutes();
  var second = time.getSeconds();
  month = month < 10 ? "0" + month : month; 
  date = date < 10 ? "0" + date : date; 
  hour = hour < 10 ? "0" + hour : hour; 
  minute = minute < 10 ? "0" + minute : minute; 
  second = second < 10 ? "0" + second : second; 
  var arr = [ 
      year + "-" + month + "-" + date, 
      year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second, 
      year + "年" + month + "月" + date + "日", 
      year + "年" + month + "月" + date + "日" + hour + ":" + minute + ":" + second, 
      hour + ":" + minute + ":" + second 
  ] 
  return arr[type]; 
} 

// 正则表达式验证手机号码
export function validatePhoneNumber(phoneNumber) {
  // 中国大陆手机号正则表达式，11位数字，以1开头
  var reg = /^1\d{10}$/;
  
  // 使用正则表达式进行匹配
  if (reg.test(phoneNumber)) {
      return true; // 手机号码格式正确
  } else {
      return false; // 手机号码格式不正确
  }
}
// 正则表达式验证密码
export function validatePassword(password) {
  // 密码正则表达式，包含至少一个大写字母、一个小写字母、一个数字，且长度在8-20之间
  var reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,20}$/;

  // 使用正则表达式进行匹配
  if (reg.test(password)) {
      return true; // 密码格式正确
  } else {
      return false; // 密码格式不正确
  }
}
export function getTimeDiff(time,day){
  var currDate = new Date();
  var createTime = new Date(time);
  createTime.setDate(createTime.getDate() + day);
  var timeDiff = createTime.getTime() - currDate.getTime(); // 时间差的毫秒数
  return timeDiff;
}

export function getStatusList(){
  let statusList=["待支付","待发货","待收货","待确认","","待评价","退货审核","取消交易","已评价","","取消订单审核","","退货失败","","","退货未发货","","退款已发货","","","退货已退款"]
  return statusList;
}




/**
 * 格式化日期字符串
 * @param {Date} date - 需要格式化的日期对象
 * @param {number} [format=1] - 日期格式,1 为 'yyyy-MM-dd'，2 为 'yyyy-MM'
 * @returns {string} 格式化后的日期字符串
 */
export function formatDate(date, format = 1) {
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, '0');
  const day = String(date.getDate()).padStart(2, '0');

  if (format === 2) {
    return `${year}-${month}`;
  } else {
    return `${year}-${month}-${day}`;
  }
}
