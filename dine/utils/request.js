const baseURL = 'https://devdine.fairycs.club';
// const baseURL = 'https://appdine.fairycs.club';
// const baseURL = 'http://localhost:8080';
// const baseURL = 'https://192.168.0.112:443';
// 封装数据请求request

export function request(params) {
  let dataObj = params.data || {};
  let access_token = '';
  //设置access_toekn
  if (getApp().globalData.access_token) {
    access_token = getApp().globalData.access_token
  } else if (wx.getStorageSync("access_token")) {
    getApp().globalData.access_token=wx.getStorageSync("access_token")
    access_token = wx.getStorageSync("access_token")
  }
  let headerObj = {
    'content-type': 'application/json',
    'access_token': access_token
  }
  return new Promise((resolve, reject) => {
    wx.showLoading({ // 请求提示
      title: '正在加载中...',
    })
    wx.request({
      url: baseURL + params.url,
      method: params.method || "GET",
      data: dataObj,
      header: headerObj,
      dataType: 'json', // 返回数据类型
      success: (res) => { // 成功调用
        // console.log(res);
        //如果返回的access_token与现在的不一样则替换
        console.log(res);
        let new_access_token = res.header.access_token
        if (new_access_token && new_access_token != getApp().globalData.access_toekn) {
          getApp().globalData.access_toekn = new_access_token
          wx.setStorageSync('access_token', new_access_token);
        }
        wx.hideLoading(); // 关闭请求提示
        // debugger
        if (res.data.code == 401) {
          wx.showToast({
            title: '登陆已过期,请重新登录',
            icon:'none'
          })
          
        }
        if (res.data.code==500) {
          wx.showToast({
            title: res.data.msg,
            icon:'none'
          })
        }
        resolve(res.data) // 成功处理res.data中的数据
      },
      fail: (err) => { // 失败调用
        wx.hideLoading(); // 关闭请求提示
        console.log(err);
        wx.showToast({ // 提示错误信息
          title: err.errMsg || '请求错误！',
          icon: 'none'
        })
        reject(err) // 失败处理err
      }
    })
  })
}