// pages/admin/categoryAdmin/add/add.js
import {
  addCategories,modifyCategories,deleteCategories,getCategoryByCategoryId
} from "@api/ProductCategoryApi"
import {
  uploadObject
} from '@api/UploadApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    categoryInfo: {
      type:1
    },
    fileList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let categoryId = options.categoryId;
   if (categoryId) {
    getCategoryByCategoryId(categoryId).then(res=>{
      let {fileList} = this.data
      fileList.push({url:res.data.picUrl})
      if (res.code==200) {
        this.setData({
          categoryInfo:res.data,
          fileList
        })
      }
    })
   }
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  submit(e) {
    let type = e.detail.target.dataset.type;
    let {categoryCode,categoryName} = e.detail.value;
    let {picUrl} = this.data.categoryInfo
    e.detail.value.picUrl=picUrl
    if (!categoryCode) {
      wx.showToast({
        title: '分类编码不能空',
        icon: "none"
      })
      return;
    }
    if (!categoryName) {
      wx.showToast({
        title: '分类名称不能空',
        icon: "none"
      })
      return;
    }
    // if (!picUrl) {
    //   wx.showToast({
    //     title: '图片不能空',
    //     icon: "none"
    //   })
    //   return;
    // }
    if (type == 'add') {
      addCategories(e.detail.value).then(res => {
        if (res.code == 200) {
          wx.showToast({
            title: '新增成功',
          })
          wx.navigateBack()
        } else if (res.code == 500) {
          wx.showToast({
            title: res.msg,
            icon: "none"
          })
        }
      })
    } else if (type == 'modify') {
      modifyCategories(e.detail.value).then(res => {
        if (res.code == 200) {
          wx.showToast({
            title: '修改成功',
          })
          wx.navigateBack()
        } else if (res.code == 500) {
          wx.showToast({
            title: res.msg,
            icon: "none"
          })
        }
      })
    } else if (type == 'delete') {
      let id = e.detail.value.categoryId;
      deleteCategories(id).then(res=>{
        if (res.code == 200) {
          wx.showToast({
            title: '删除成功',
          })
          wx.navigateBack()
        } else if (res.code == 500) {
          wx.showToast({
            title: res.msg,
            icon: "none"
          })
        }
      })
    }
  },
  afterRead(event) {
    const {
      file
    } = event.detail;
    file.type = 'category'
    // 当设置 mutiple 为 true 时, file 为数组格式，否则为对象格式
    uploadObject(file).then(res => {
      const {
        fileList = [],categoryInfo
      } = this.data;
      
      categoryInfo.picUrl = res.msg;
      let split = categoryInfo.picUrl.split("/");
      let name = split[split.length - 1]
      const length = name.indexOf("?");
      name = name.substring(0, length);
      categoryInfo.picName = name;
      fileList.push({
        url: res.msg
      });
      this.setData({
        fileList,
        categoryInfo
      });
      console.log(this.data.categoryInfo);
    })
  },
  delete(event) {
    let list = this.data.fileList;
    list.splice(event.detail.index, 1);
    this.setData({
      fileList: list
    })
  },
  filedChange(e){
    let {type} =e.currentTarget.dataset
    let value = e.detail
    let {categoryInfo} = this.data
    if (type==='code') {
      categoryInfo.categoryCode=value
    }else if(type==='name'){
      categoryInfo.categoryName=value
    }else if(type==='dineType'){
      categoryInfo.dineType=value
    }
    this.setData({
      categoryInfo
    })
    console.log(this.data);
  }
})