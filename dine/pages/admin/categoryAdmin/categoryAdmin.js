// pages/admin/categoryAdmin/categoryAdmin.js
import {getCategories} from "../../../api/ProductCategoryApi"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    categories: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getCategories();
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getCategories();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  toModify(e) {
    wx.navigateTo({
      url: '/pages/admin/categoryAdmin/add/add?categoryId='+e.currentTarget.dataset.categoryid
      +'&categoryCode='+e.currentTarget.dataset.categorycode
      +'&categoryName='+e.currentTarget.dataset.categoryname,
    })
  },
  toAdd(){
    wx.navigateTo({
      url: '/pages/admin/categoryAdmin/add/add',
    })
  },
  getCategories(){
    getCategories().then(res=>{
      let categories = res.data;
      this.setData({
        categories
      })
    })
  }
})