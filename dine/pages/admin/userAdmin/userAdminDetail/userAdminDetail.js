import {
  getDetailInPersonPage,
  modifyUserInfo
} from '@api/UserInfoApi'
import {
  dictByCondition
} from '@api/DictApi'
Page({
  data: {
    userType: null,
    userTypeList: [],
    userInfo: null,
    show: false
  },
  async onLoad(e) {
    let {
      data: userInfo
    } = await getDetailInPersonPage(e.userId);
    let {
      data
    } = await dictByCondition({
      parentCode: 'user_type'
    });
    let userType = this.data.userType;
    let userTypeList = this.data.userTypeList;
    userType = new Map()
    data.forEach((item) => {
      userType.set(Number(item.value), item.name);
      userTypeList.push({
        id: item.value,
        name: item.name
      })
    })
    userInfo.userTypeName = userType.get(userInfo.userType);
    this.setData({
      userTypeList,
      userType,
      userInfo
    })
  },
  true() {

  },
  showPop() {
    if(getApp().globalData.userInfo.userType==100){
      this.setData({
        show: true
      })
      return;
    }
    if (this.data.userInfo.userType != 0) {
      this.setData({
        show: true
      })
    }else{
      wx.showToast({
        title: '对方为管理员不能设置',
        icon:'none'
      })
    }
  },
  onCancel() {
    this.setData({
      show: false
    })
  },
  onClickConfirm(e) {
    let userInfo = this.data.userInfo;
    userInfo.userType = e.detail.value.id
    userInfo.userTypeName = this.data.userType.get(Number(userInfo.userType));
    this.setData({
      show: false,
      userInfo
    })
  },
  async modifyUserInfo() {
    let {
      code
    } = await modifyUserInfo({
      userId: this.data.userInfo.userId,
      userType: this.data.userInfo.userType
    })
    if (code === 200) {
      wx.showToast({
        title: '修改成功',
        success: () => {
          const page = getCurrentPages()
          if (page.length > 1) {
            var prePage = page[page.length - 2]
            console.log(this.data.userInfo);
            prePage.searchUserByPhone({
              detail: this.data.userInfo.mobilePhone
            })
          }
          wx.navigateBack()
        }
      })
    }
  }
})