import {
  getUserByPhone
} from '@api/UserInfoApi'
Page({
  data: {
    phone: null,
    userInfo: {},
    userType: null,
  },
  onLoad() {
    let userType = this.data.userType;
    userType = new Map()
    userType.set(0, "管理员");
    userType.set(1, "普通用户");
    userType.set(2, "厨师");
    this.setData({
      userType
    })
  },
  async searchUserByPhone(e) {
    console.log(e);
    let {
      data: userInfo
    } = await getUserByPhone(e.detail)
    let userType = this.data.userType;
    if (userInfo) {
      userInfo.userTypeName = userType.get(userInfo.userType)
      this.setData({
        userInfo
      })
    }
  },
  toDetail(){
    wx.navigateTo({
      url: '/pages/admin/userAdmin/userAdminDetail/userAdminDetail?userId='+this.data.userInfo.userId,
    })
  }
})