// pages/admin/skuTypeAdmin/add/add.js
import {
  getCategories
} from "../../../../api/ProductCategoryApi"
import {
  getTypeById,
  add,
  modify,
  deleteById
} from "../../../../api/SkuTypeApi"
//skuTypeValue
import {
  addTypeValue,
  modifyTypeValue,
  deleteTypeValueById
} from "../../../../api/SkuTypeValueApi"
import {
  getResult,
  getResultAndBack
} from "../../../../utils/result"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    deleteDialog: false,
    showDialog: false,
    sku: {},
    categoryList: [],
    show: false,
    currentIndex: [],
    currentAddValue: "",
    currentAddValueId: "",
    currentDeleteIds: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    //获取分类信息
    getCategories().then(res => {
      let categoryList = res.data;
      this.setData({
        categoryList
      })
    });
    //获取详情
    if (options.typeId) {
      getTypeById(options.typeId).then(res => {
        let sku = this.data.sku;
        sku = res.data
        this.setData({
          sku
        })
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {


  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  //弹框确认
  onClickConfirm(e) {
    let categoryId = e.detail.value.categoryId;
    let categoryName = e.detail.value.categoryName;
    let sku = this.data.sku;
    sku.categoryId = categoryId;
    sku.categoryName = categoryName;
    this.setData({
      show: false,
      sku
    })
  },
  //关闭弹框
  onCancel() {
    this.setData({
      show: false
    })
  },
  //点击分类名称
  onClickCategory() {
    this.setData({
      show: true
    })
  },
  //规格新增/更新/删除
  submit(e) {
    let categoryId = e.detail.value.categoryId;
    let typeName = e.detail.value.typeName;
    if (!categoryId) {
      wx.showToast({
        title: '分类名称不能空',
        icon: "none"
      })
      return;
    }
    if (!typeName) {
      wx.showToast({
        title: '规格名称不能空',
        icon: "none"
      })
      return;
    }
    let type = e.detail.target.dataset.type;
    if (type == 'add') {
      add(e.detail.value).then(res => {
        getResultAndBack(res, '新增')
      })
    } else if (type == 'modify') {
      modify(e.detail.value).then(res => {
        getResultAndBack(res, '修改')
      })
    } else if (type == 'delete') {
      deleteById(e.detail.value.typeId).then(res => {
        getResultAndBack(res, '删除')
      })
    }
  },
  onChange(event) {
    this.setData({
      currentIndex: event.detail,
    });
  },
  //点击新增按钮
  clickOnAddTypeValue() {
    this.setData({
      showDialog: true
    })
  },
  //点击修改按钮
  clickOnModifyTypeValue() {
    if (this.data.currentIndex.length > 1) {
      wx.showToast({
        title: '请选择一条数据',
        icon: "none"
      });
      return;
    }
    let index = this.data.currentIndex[0];
    let value = this.data.sku.value[index];
    this.setData({
      currentAddValueId: value.valueId,
      currentAddValue: value.typeValue,
      showDialog: true
    })
  },
  clickOnDeleteTypeValue() {
    let indexs = this.data.currentIndex;
    let deleteIds = [];
    for (var i = 0; i < indexs.length; i++) {
      deleteIds.push(this.data.sku.value[indexs[i]].valueId);
    }
    this.setData({
      currentDeleteIds: deleteIds,
      deleteShow: true,
    })
  },
  //确认新增/更新
  addTypeValue(e) {
    let typeValueId = e.target.dataset.valueid;
    let typeValue = e.target.dataset.value;
    let typeId = this.data.sku.typeId;
    let skuTypeValue = {};
    skuTypeValue.typeValue = typeValue;
    skuTypeValue.typeId = typeId;
    skuTypeValue.valueId = typeValueId;
    if (!typeValue) {
      wx.showToast({
        title: '属性值不能为空',
        icon: "none"
      })
    }
    //修改
    if (typeValueId) {
      modifyTypeValue(skuTypeValue).then(res => {
        if (res.code == 200) {
          wx.showToast({
            title: '修改成功',
          })
          getTypeById(typeId).then(res => {
            let sku = this.data.sku
            sku = res.data
            this.setData({
              currentAddValue: null,
              sku
            })
          })
        } else if (res.code == 500) {
          wx.showToast({
            title: res.msg,
            icon: "none"
          })
        }
      })
    } else {
      addTypeValue(skuTypeValue).then(res => {
        if (res.code == 200) {
          wx.showToast({
            title: '新增成功',
          })
          getTypeById(typeId).then(res => {
            let sku = this.data.sku
            sku = res.data
            this.setData({
              currentAddValue: null,
              sku
            })
          })
        } else if (res.code == 500) {
          wx.showToast({
            title: res.msg,
            icon: "none"
          })
        }
      })
    }
  },
  deleteTypeValue() {
    let ids = this.data.currentDeleteIds;
    deleteTypeValueById(ids).then(res => {
      if (res.code == 200) {
        wx.showToast({
          title: '删除成功',
        })
        let sku =this.data.sku;
        getTypeById(this.data.sku.typeId).then(res => {
          sku = res.data
          this.setData({
            currentAddValue: null,
            sku
          })
        })
      } else if (res.code == 500) {
        wx.showToast({
          title: res.msg,
          icon: "none"
        })
      }
    })
  },
  //关闭弹框
  onCloseDialog() {
    this.setData({
      currentAddValue: "",
      currentAddValueId: "",
      currentDeleteIds: "",
      deleteDialog: false,
      showDialog: false
    })
  },
  //弹框输入实时改变
  onChangeAdd(e) {
    this.setData({
      currentAddValue: e.detail
    })
  },true(){}
})

