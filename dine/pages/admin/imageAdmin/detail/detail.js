import {
  getById,
  savePicture,
  updatePicture
} from '@api/PictureManageApi'
import {
  uploadObject
} from '@api/UploadApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    fileList: [],
    pickerShow: false,
    purposeList: [{
        type: 2,
        text: '首页-轮播图'
      },
      {
        type: 3,
        text: '首页-广告图'
      },
      {
        type: 4,
        text: '个人-广告图'
      },
      {
        type: 5,
        text: '商城-展示图'
      },
      {
        type: 6,
        text: '个人-客服微信'
      },
    ],
    pictureInfo: {
      pictureId: '',
      picName: '',
      picUrl: '',
      description: '',
      imageType: 0,
      width: 0,
      height: 0,
      pageUrl: '',
      state: 1,
    },
    showSizeFlag: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    let {
      pictureId
    } = options
    if (pictureId) {
      let {
        data
      } = await getById(pictureId);
      let {
        fileList
      } = this.data
      fileList.push({
        url: data.picUrl
      });
      this.setData({
        pictureInfo: data,
        fileList
      })
    }
  },
  //用途框-取消
  onCancel() {
    this.setData({
      pickerShow: false
    })
  },
  //用途框-确认
  onClickConfirm(e) {
    let {
      type,
      text
    } = e.detail.value
    this.setData({
      ['pictureInfo.imageType']: type,
      ['pictureInfo.description']: text,
      pickerShow: false,
      ['pictureInfo.picName']: text,
    })
  },
  //显示用途框
  showPicker() {
    this.setData({
      pickerShow: true
    })
  },
  fieldChange(e) {
    let {
      type
    } = e.currentTarget.dataset
    let value = e.detail
    if (type == 'pageUrl') {
      this.setData({
        ['pictureInfo.pageUrl']: value
      })
    } else if (type == 'width') {
      this.setData({
        ['pictureInfo.width']: value
      })
    } else if (type == 'height') {
      this.setData({
        ['pictureInfo.height']: value
      })
    } else if (type == 'state') {
      this.setData({
        ['pictureInfo.state']: value
      })
    }

  },
  //图片
  afterRead(event) {
    const {
      file
    } = event.detail;
    file.type = 'pic'
    // 当设置 mutiple 为 true 时, file 为数组格式，否则为对象格式
    uploadObject(file).then(res => {
      const {
        fileList = [], pictureInfo
      } = this.data;
      pictureInfo.picUrl = res.msg;
      fileList.push({
        url: res.msg
      });
      this.setData({
        fileList,
        pictureInfo
      });
    })
  },
  //图片删除
  delete(e) {
    let list = this.data.fileList;
    list.splice(e.detail.index, 1);
    this.setData({
      fileList: list,
      ['pictureInfo.picUrl']: '',
    })
  },
  //保存按钮
  async saveOrUpdate() {
    let {
      pictureInfo
    } = this.data
    if (isEmpty(pictureInfo.pictureId)) {
      let {
        code,
        msg
      } = await savePicture(pictureInfo)
      if (code == 200) {
        wx.showToast({
          title: msg,
          icon: 'none'
        })
        wx.navigateBack({
          delta: 1, // 返回的页面数，1 表示返回上一页
          success: function () {
            // 刷新页面
            let pages = getCurrentPages();
            let currentPage = pages[pages.length - 1];
            currentPage.onLoad(); // 执行页面加载操作
          }
        });
      } else if (code == 500) {
        wx.showToast({
          title: msg,
          icon: 'error'
        })
      }
    } else {
      let {
        code,
        msg
      } = await updatePicture(pictureInfo)
      if (code == 200) {
        wx.showToast({
          title: msg,
          icon: 'none'
        })
        wx.navigateBack({
          delta: 1, // 返回的页面数，1 表示返回上一页
          success: function () {
            // 刷新页面
            let pages = getCurrentPages();
            let currentPage = pages[pages.length - 1];
            currentPage.onLoad(); // 执行页面加载操作
          }
        });
      } else if (code == 500) {
        wx.showToast({
          title: msg,
          icon: 'error'
        })
      }
    }
  },
  showSize(){
    this.setData({
      showSizeFlag:true
    })
  },
  true(){}
})

function isEmpty(obj) {
  if (obj === null) return true;
  if (typeof obj === 'undefined') {
    return true;
  }
  if (typeof obj === 'string') {
    if (obj === "") {
      return true;
    }
    var reg = new RegExp("^([ ]+)|([　]+)$");
    return reg.test(obj);
  }
  return false;
}