import {getList} from '../../../api/PictureManageApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pictureList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    let {data} = await getList({current:1,size:999});
    this.setData({
      pictureList:data
    })
  },

  toDetail(e){
    let {pictureid} = e.currentTarget.dataset;
    if (pictureid) {
      wx.navigateTo({
        url: './detail/detail?pictureId='+pictureid,
      })
    }else{
      wx.navigateTo({
        url: './detail/detail',
      })
    }
   
  }
})