import {
  getPageListByAdmin
} from '../../../api/OrderMasterApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //订单状态(0-待支付，1-待发货，2-待收货，3-待确认,5-待评价，6-退货审核，7-取消交易，8-已评价,12-退货失败，15-退货未发货，17-退款已发货，20-退货已退款)
    option1: [{
        text: '全部',
        value: -1,
        icon: ''
      },
      {
        text: '客户待支付',
        value: 0,
        icon: ''
      },
      {
        text: '商家待发货',
        value: 1,
        icon: ''
      },
      {
        text: '客户待收货',
        value: 2,
        icon: ''
      },
      {
        text: '客户待确认',
        value: 3,
        icon: ''
      },
      {
        text: '待评价',
        value: 5,
        icon: ''
      },
      {
        text: '退货审核',
        value: 6,
        icon: ''
      },
      {
        text: '取消交易',
        value: 7,
        icon: ''
      },
      {
        text: '已评价',
        value: 8,
        icon: ''
      },
      {
        text: '取消审核',
        value: 10,
        icon: ''
      },
      {
        text: '退货失败',
        value: 12,
        icon: ''
      },
      {
        text: '客户退货未发货',
        value: 15,
        icon: ''
      },
      {
        text: '客户退款已发货',
        value: 17,
        icon: ''
      },
      {
        text: '退货已退款',
        value: 20,
        icon: ''
      },
    ],
    option2: [{
        text: '默认排序',
        value: 0,
        icon: ''
      },
      {
        text: '更新倒序',
        value: 1,
        icon: ''
      },
    ],
    //下拉框左侧激活字段
    leftValue: -1,
    //下拉框右侧激活字段
    rightValue: 0,
    //搜索关键词
    keyword: '',
    queryParam: {
      current: 1,
      size: 999,
      entity: {}
    },
    orderList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {




  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  //子组件刷新父组件页面
  async rollbackTabRefresh(e) {
    let orderStatus = e.detail;
    let queryParam = this.data.queryParam;
    queryParam.entity.orderStatus = orderStatus;
    //返回“全部”页面
    if (orderStatus = -1) {
      queryParam.entity.orderStatus = null;
    }
    let {
      data
    } = await getPageList(this.data.queryParam);
    //重置搜索条件
    this.setData({
      queryParam,
      orderList: data
    })
  },
  //下拉左侧
  leftChange(e) {
    console.log(e);
    this.setData({
      leftValue: e.detail
    })
    console.log(this.data.leftValue);
  },
  //下拉右侧
  rightChange(e) {
    this.setData({
      rightValue: e.detail
    })
  },
  //搜索框
  keywordChange(e) {
    this.setData({
      keyword: e.detail
    })
  },
  async search() {
    let {
      leftValue,
      rightValue,
      keyword,
      queryParam
    } = this.data
    queryParam.entity['orderStatus'] = leftValue
    queryParam.entity['orderByAdmin'] = rightValue
    queryParam.entity['keyword'] = keyword
    console.log(queryParam);
    let {
      data,
      code
    } = await getPageListByAdmin(queryParam);
    if (code == 200) {
      this.setData({
        queryParam,
        orderList: data
      })
    }
  }
})