import {
  getOrderDetailByOrderId,
  updateOrder
} from '@api/OrderMasterApi'
import {
  getProcessByOrderId
} from '@api/OrderStatusLogsApi'
import {
  getStatusList,
  timestampToTime
} from '@utils/util'
import {
  uploadObject
} from '@api/UploadApi'
import {savePicture} from '@api/PictureManageApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    fileList: [],
    shippingDialogShow: false,
    currShippingCompany: '',
    currShippingSn: '',
    refundTypeList: ["", "七天无理由退货", "商品有瑕疵", "快递运送破损", "不喜欢", "其他", "收货地址拍错", "规格/数量/款式拍错", "暂时不需要"],
    dialogShow: false,
    orderMaster: {},
    modifyMoneyFlag: false,
    modifyType: '', //1-商品总价 2-运费,
    modifyTemValue: '',
    process: [],
    activeNames: ['1'],
    logisticsPicUrl: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    let {
      orderId
    } = options;
    let {
      data
    } = await getOrderDetailByOrderId(orderId);
    let fileList = this.data.fileList;
    fileList.push({
      url: data.logisticsPicUrl
    });
    this.setData({
      fileList,
      orderMaster: data
    })
    this.getOrderStatusLog(orderId);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  //弹出填写快递框
  sendShipping() {
    this.setData({
      shippingDialogShow: true
    })
  },
  fieldChange(e) {
    let type = e.currentTarget.dataset.type;
    let value = e.detail
    if (type == 'company') {
      this.setData({
        currShippingCompany: value,
      })
    } else {
      this.setData({
        currShippingSn: value,
      })
    }
  },
  //填写快递
  async confirmSend() {

    let {
      currShippingCompany,
      currShippingSn
    } = this.data
    let orderMaster = this.data.orderMaster
    orderMaster.previousStatus = orderMaster.orderStatus
    orderMaster.orderStatus = 2
    orderMaster.shippingCompName = currShippingCompany
    orderMaster.shippingSn = currShippingSn
    let {
      code,
      msg
    } = await updateOrder(orderMaster)
    if (code == 200) {
      this.setData({
        shippingDialogShow: false
      })
      let {
        data
      } = await getOrderDetailByOrderId(orderMaster.orderId);
      this.setData({
        orderMaster: data
      })
    }
  },
  //不同意按钮
  disAgree() {
    this.setData({
      dialogShow: true
    })
  },
  //填写不同意内容后
  async afterWriteRefundReason() {
    let orderMaster = this.data.orderMaster
    orderMaster.previousStatus = orderMaster.orderStatus
    orderMaster.orderStatus = 12
    let {
      code
    } = await updateOrder(orderMaster)
    if (code == 200) {
      const page = getCurrentPages()
      if (page.length > 1) {
        var prePage = page[page.length - 2]
        prePage.search()
      }
      wx.navigateBack()
    }
  },
  //同意按钮
  async agree() {
    let orderMaster = this.data.orderMaster
    orderMaster.previousStatus = orderMaster.orderStatus
    orderMaster.orderStatus = 15
    let {
      code
    } = await updateOrder(orderMaster)
    if (code == 200) {
      const page = getCurrentPages()
      if (page.length > 1) {
        var prePage = page[page.length - 2]
        prePage.search()
      }
      wx.navigateBack()
    }
  },
  //填写拒绝原因
  textareaInput(e) {
    this.setData({
      ['orderMaster.rejectReason']: e.detail.value
    })
  },
  //修改金额弹框
  modifyMoney(e) {
    let type = e.currentTarget.dataset.type
    let modifyTemValue = 0;
    if (type == 1) {
      modifyTemValue = this.data.orderMaster.orderMoney
    } else {
      modifyTemValue = this.data.orderMaster.feeMoney
    }
    this.setData({
      modifyMoneyFlag: true,
      modifyType: e.currentTarget.dataset.type,
      modifyTemValue,
    })
  },
  //修改金额框输入变化
  modifyTemValueChange(e) {
    this.setData({
      modifyTemValue: e.detail
    })
  },
  //修改金额框提交
  async moneyConfrim() {
    debugger
    let {
      modifyType,
      orderMaster,
      modifyTemValue
    } = this.data
    let newOrderMaster = {
      orderId: orderMaster.orderId
    }
    //修改商品总价
    if (modifyType == 1) {
      let v1 = parseFloat(modifyTemValue)
      newOrderMaster.orderMoney = v1
      let v2 = parseFloat(orderMaster.feeMoney)
      newOrderMaster.paymentMoney = v1 + v2
    }
    //修改运费
    else {
      let v1 = parseFloat(modifyTemValue)
      newOrderMaster.feeMoney = v1
      let v2 = parseFloat(orderMaster.orderMoney)
      newOrderMaster.paymentMoney = v1 + v2
    }
    updateOrder(newOrderMaster);
  },
  //修改金额框取消
  moneyCancel() {
    this.setData({
      modifyTemValue: '',
      modifyMoneyFlag: false
    })
  },
  //管理员确认快递已送到客户
  async customerReceive() {
    let {
      code
    } = await updateOrder({
      orderId: this.data.orderMaster.orderId,
      previousStatus: this.data.orderMaster.orderStatus,
      orderStatus: 3
    })
    if (code == 200) {
      const page = getCurrentPages()
      if (page.length > 1) {
        var prePage = page[page.length - 2]
        prePage.search()
      }
      wx.navigateBack()
    }
  },
  /**
   * 取消审核
   */
  //不同意按钮
  cancelDisagree() {
    this.setData({
      cancelDialogShow: true
    })
  },
  //填写拒绝原因
  cancelTextareaInput(e) {
    this.setData({
      ['orderMaster.cancelRejectReason']: e.detail.value
    })
  },
  //填写不同意内容后
  async afterWriteCancelRefundReason() {
    let orderMaster = this.data.orderMaster
    orderMaster.previousStatus = orderMaster.orderStatus
    orderMaster.orderStatus = 1 //回滚到待收货
    let {
      code
    } = await updateOrder(orderMaster)
    if (code == 200) {
      const page = getCurrentPages()
      if (page.length > 1) {
        var prePage = page[page.length - 2]
        prePage.search()
      }
      wx.navigateBack()
    }
  },
  //同意按钮
  async cancelAgree() {
    let orderMaster = this.data.orderMaster
    orderMaster.previousStatus = orderMaster.orderStatus
    orderMaster.orderStatus = 7 //取消订单
    let {
      code
    } = await updateOrder(orderMaster)
    if (code == 200) {
      const page = getCurrentPages()
      if (page.length > 1) {
        var prePage = page[page.length - 2]
        prePage.search()
      }
      wx.navigateBack()
    }
  },
  //退货商品确认
  async RefundProductReceive() {
    let orderMaster = this.data.orderMaster
    orderMaster.previousStatus = orderMaster.orderStatus
    orderMaster.orderStatus = 20 //取消订单
    let {
      code
    } = await updateOrder(orderMaster)
    if (code == 200) {
      const page = getCurrentPages()
      if (page.length > 1) {
        var prePage = page[page.length - 2]
        prePage.search()
      }
      wx.navigateBack()
    }
  },
  /**
   * 获取订单状态变更
   * @param {*} orderId 
   */
  async getOrderStatusLog(orderId) {
    let {
      data
    } = await getProcessByOrderId(orderId);
    let {
      process
    } = this.data
    let statusList = getStatusList();
    data.forEach(item => {
      let time = timestampToTime(item.changeDate, 1)
      let status = statusList[item.newStatus];
      process.push({
        text: status,
        desc: time
      })
    })
    this.setData({
      process
    })
  },
  //折叠框
  onChange(event) {
    this.setData({
      activeNames: event.detail,
    });
  },
  //图片删除
  delete(e) {
    let list = this.data.fileList;
    list.splice(e.detail.index, 1);
    this.setData({
      fileList: list,
      ['pictureInfo.picUrl']: '',
    })
  },
  afterRead(event) {
    const {
      file
    } = event.detail;
    file.type = 'logistics'
    // 当设置 mutiple 为 true 时, file 为数组格式，否则为对象格式
    uploadObject(file).then(res => {
      const {
        fileList = [], orderMaster
      } = this.data;
      orderMaster.logisticsPicUrl = res.msg;
      fileList.push({
        url: res.msg
      });
      this.setData({
        fileList,
        orderMaster
      });
      //添加图片
      savePicture({
        picName:'物流截图',
        picUrl:res.msg,
        description:this.data.orderMaster.orderSn, //订单号
        imageType:7,
        state:1,
        version:1,
        createdTime:new Date(),
        updateTime:new Date(),
        userId:this.data.orderMaster.userId,
      }).then(res =>{
        wx.showToast({
          title: '成功',
        })
      })
    })
  }
})