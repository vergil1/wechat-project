// pages/admin/admin.js
import {getDealCount} from '@api/OrderMasterApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    count:0,
    userInfo:{}
  },

  async onLoad(){
    let {data,code} = await getDealCount()
    if (code==200) {
      this.setData({
        count:data,
        userInfo:getApp().globalData.userInfo
      })
    }
  }
})