import * as util from '@utils/util'
import * as statisticsApi from '@api/StatisticsApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    timeText: "",
    currentDate: new Date().getTime(),
    rs:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let date = new Date();
    let timeText = util.formatDate(date,2);
    this.getData(timeText)
    this.setData({
      timeText
    })

  },
  monthSelect() {
    this.setData({
      monthShow: true
    })
  },
  async getData(date){
    let {data} =await statisticsApi.getEachDayTurnoverByMonth(date);
    this.setData({
      rs:data
    })
  },
   //月份确认
   async monthConfirm(e) {
    let date = new Date(e.detail)
    let timeText = util.formatDate(new Date(date), 2);
    //查询
    await this.getData(timeText);
    this.setData({
      monthShow: false,
      timeText
    })
  },
  monthCancel() {
    this.setData({
      monthShow: false
    })
  },
})