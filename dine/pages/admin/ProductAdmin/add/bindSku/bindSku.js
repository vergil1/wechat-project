// pages/admin/ProductAdmin/add/bindSku/bindSku.js
import {getListByProductId} from '../../../../../api/ProductSkuApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    skuInfo:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      productId: options.productId
    })
    this.search(options.productId)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  add() {
    wx.navigateTo({
      url: './addSku/addSku?productId=' + this.data.productId,
    })
  },

  //搜索方法
  search(productId){
    getListByProductId(productId).then(res=>{
      if(res.code==200){
        this.setData({
          skuInfo:res.data
        })
      }else{
        wx.showToast({
          title: res.msg,
          icon:'none'
        })
      }
    })
  },
  toModify(e){
    let skuId = e.currentTarget.dataset.skuid;
    wx.navigateTo({
      url: './addSku/addSku?productId=' + this.data.productId+"&skuId="+skuId,
    })
    }
})
