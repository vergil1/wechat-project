// pages/admin/ProductAdmin/add/bindSku/addSku/addSku.js
import {
  queryTypeClassByProductId
} from '@api/SkuTypeApi'
import {
  uploadObject
} from '@api/UploadApi'
import {addProductSku,modifyProductSku,getDetailByProductId} from '@api/ProductSkuApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //级联框显示值
    showValue: [],
    //级联框自定义字段
    fieldNames: {
      text: 'name',
      value: 'id',
      children: 'values',
    },
    currentIndex: -1,
    categories: [],
    showType: false,
    entity: [],
    fileList: [],
    skuInfo: {
      price: 1,
      store: 1,
      picUrl: '',
      skuId: '',
      skuPicInfo:{}
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const {productId,skuId} = options;
    queryTypeClassByProductId(productId).then(res => {
      const {
        data
      } = res
      const {skuInfo,showValue} = this.data
      skuInfo.productId=productId
      data.forEach((item, index) => {
        item.productId = productId;
      })
      this.setData({
        categories: data,
        skuInfo
      })
    })
    if (skuId) {
      getDetailByProductId(skuId).then(res=>{
        if (res.code==200) {
          console.log(res.data);
          //返回结果
          let {data} = res
          //页面参数
          const {
            fileList = [],showValue
          } = this.data;
          fileList.push({
            url: data.picUrl
          });
          data.entity.forEach((item,index)=>{
            showValue.push(item.typeName+":"+item.typeValue)
          })
        
          this.setData({
            skuInfo:data,
            fileList,
            entity:data.entity,
            showValue
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  //新增规格按钮
  addEntity() {
    const {
      entity
    } = this.data
    let item = {
      productId: this.data.productId
    };
    entity.push(item);
    this.setData({
      entity,
    })
    console.log(this.selectComponent("#van-cascader"))
  },
  //删除按钮
  closeSwipe(e) {
    let index = e.currentTarget.dataset.index;
    let {
      entity,
      showValue
    } = this.data;
    entity.splice(index, 1)
    showValue.splice(index, 1)
    this.setData({
      entity,
      showValue
    })

  },
  //属性选择框
  chooseSkuType(e) {
    let index = e.currentTarget.dataset.index;
    this.setData({
      showType: true,
      currentIndex: index,
    })
    console.log(this.selectComponent("#van-cascader"))
    this.selectComponent("#van-cascader").innerValue = 0
    console.log(this.selectComponent("#van-cascader"))
  },
  //级联框选完
  onFinish(e) {
    const {
      selectedOptions,
      value
    } = e.detail;
    const fieldValue = selectedOptions
      .map((option) => option.name)
      .join(':');
    const {
      showValue,
      currentIndex,
      entity
    } = this.data
    showValue[currentIndex] = fieldValue
    let typeId = selectedOptions[0].id
    let valueId = selectedOptions[1].id
    entity[currentIndex] = {
      typeId: typeId,
      valueId: valueId
    }
    this.setData({
      showValue,
      cascaderValue: value,
      showType: false,
      entity
    })
    console.log(this.selectComponent("#van-cascader"))
    this.selectComponent("#van-cascader").setData({
      innerValue: -1
    })
    console.log(this.selectComponent("#van-cascader"))
  },
  //级联框取消
  onClose() {
    this.setData({
      showType: false,
    });
  },
  //图片
  afterRead(event) {
    const {
      file
    } = event.detail;
    file.type = 'skuPic'
    // 当设置 mutiple 为 true 时, file 为数组格式，否则为对象格式
    uploadObject(file).then(res => {
      const {
        fileList = [], skuInfo
      } = this.data;
      
      skuInfo.skuPicInfo.picUrl = res.msg;
      let split = skuInfo.skuPicInfo.picUrl.split("/");
      let name = split[split.length - 1]
      const length = name.indexOf("?");
      name = name.substring(0, length);
      skuInfo.skuPicInfo.picName = name;
      fileList.push({
        url: res.msg
      });
      this.setData({
        fileList,
        skuInfo
      });
    })
  },
  delete(event) {
    let list = this.data.fileList;
    list.splice(event.detail.index, 1);
    this.setData({
      fileList: list
    })
  },
  //表单组件变化
  onChange(e) {
    let type = e.currentTarget.dataset.type;
    let skuInfo = this.data.skuInfo;
    if (type == 'price') {
      let price = e.detail;
      skuInfo.price = price;
    } else if (type == 'store') {
      let store = e.detail;
      skuInfo.store = store;
    }
  },
  //增删改查
  submit(e) {
    let type = e.detail.target.dataset.type;
    let {
      skuInfo,
      entity
    } = this.data;
    const set = new Set();
  
      // if (!skuInfo.skuPicInfo.picUrl) {
      //   wx.showToast({
      //     title: '图片不能为空',
      //     icon: 'none'
      //   });
      //   return;
      // }
      let flag=false;
      entity.forEach(item => {
        if (set.has(item.typeId)) {
          flag=true;
          return flag;
        }
        set.add(item.typeId)
      })
      if (flag) {
        wx.showToast({
          title: '添加规格属性不能相同',
          icon: 'none'
        });
        return;
      }
      skuInfo.typeValues=entity;
      console.log(skuInfo);
      //新增
      if (type == 'add') {
      addProductSku(skuInfo).then(res=>{
        if (res.code==200) {
          wx.showToast({
            title: '新增成功',
            success: () => {
              const page = getCurrentPages()
              if (page.length > 1) {
                var prePage = page[page.length - 2]
                prePage.search(skuInfo.productId)
              }
              wx.navigateBack()
            }
          })
        }else if(res.code==500){
          wx.showToast({
            title: res.msg,
            icon: 'none'
          });
        }
      })
    } else if (type == 'modify') {
      console.log(skuInfo);
      modifyProductSku(skuInfo).then(res=>{
        if (res.code==200) {
          wx.showToast({
            title: '新增成功',
            success: () => {
              const page = getCurrentPages()
              if (page.length > 1) {
                var prePage = page[page.length - 2]
                prePage.search(skuInfo.productId)
              }
              wx.navigateBack()
            }
          })
        }else if(res.code==500){
          wx.showToast({
            title: res.msg,
            icon: 'none'
          });
        }
      })
    }
  }
})