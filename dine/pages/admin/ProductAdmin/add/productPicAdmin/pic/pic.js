// pages/admin/ProductAdmin/add/productPicAdmin/pic/pic.js
import {
  getSingleById,
  addPicInfo,
  modifyPicInfo,
  deletePicInfoById
} from '@api/ProductPicInfoApi'
import {
  uploadObject
} from '@api/UploadApi'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    productPicInfo: {
      productPicId: '',
      productId: '',
      picDesc: '',
      picUrl: '',
      isMaster: 0,
      picOrder: '',
      picStatus: 1,
    },
    fileList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    //修改
    if (options.productPicId) {
      getSingleById(options.productPicId).then(res => {
        let fileList = this.data.fileList;
        let file = {
          name: res.data.picName,
          url: res.data.picUrl
        }
        fileList.push(file)
        this.setData({
          productPicInfo: res.data,
          fileList
        })
      })
    } else {
      let productPicInfo = this.data.productPicInfo;
      productPicInfo.productId = options.productId;
      productPicInfo.picOrder = Number(options.maxOrder) + 1;
      this.setData({
        productPicInfo
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }

  ,
  //图片
  afterRead(event) {
    const {
      file
    } = event.detail;
    file.type = 'pic'
    // 当设置 mutiple 为 true 时, file 为数组格式，否则为对象格式
    uploadObject(file).then(res => {
      const {
        fileList = [], productPicInfo
      } = this.data;
      productPicInfo.picUrl = res.msg;
      let split = productPicInfo.picUrl.split("/");
      let name = split[split.length - 1]
      const length = name.indexOf("?");
      name = name.substring(0, length);
      productPicInfo.picName=name;
      fileList.push({
        url: res.msg
      });
      this.setData({
        fileList,
        productPicInfo
      });
      console.log(this.data.productPicInfo);
    })
  },
  //图片删除
  delete(event) {
    let list = this.data.fileList;
    list.splice(event.detail.index, 1);
    this.setData({
      fileList: list
    })
  },
  //表单修改
  onChange(e) {
    let type = e.target.dataset.type;
    let productPicInfo = this.data.productPicInfo
    if (type == 'picDesc') {
      productPicInfo.picDesc = e.detail
    } else if (type == 'isMaster') {
      productPicInfo.isMaster = e.detail
    } else if (type == 'picStatus') {
      productPicInfo.picStatus = e.detail
    } else if (type == 'picOrder') {
      productPicInfo.picOrder = e.detail
    }
    this.setData({
      productPicInfo
    })
  },
  //增删改
  submit(e) {
    let type = e.detail.target.dataset.type;
    let productPicInfo = this.data.productPicInfo
    if (type == 'add') {
      if (!productPicInfo.picUrl) {
        wx.showToast({
          title: '必须上传图片！',
          icon: 'none'
        });
        return;
      }

      addPicInfo(productPicInfo).then(res => {
        if (res.code == 200) {
          wx.showToast({
            title: '新增成功',
            success: () => {
              const page = getCurrentPages()
              if (page.length > 1) {
                var prePage = page[page.length - 2]
                prePage.searchRs(productPicInfo.productId)
              }
              wx.navigateBack()
            }
          })
        } else if (res.code == 500) {
          wx.showToast({
            title: res.msg,
            icon: 'none'
          })
        }
      })
    } else if (type == 'modify') {
      if (!productPicInfo.picUrl) {
        wx.showToast({
          title: '必须上传图片！',
          icon: 'none'
        });
        return;
      }
      modifyPicInfo(productPicInfo).then(res => {
        if (res.code == 200) {
          wx.showToast({
            title: '修改成功',
            success: () => {
              const page = getCurrentPages()
              if (page.length > 1) {
                var prePage = page[page.length - 2]
                prePage.searchRs(productPicInfo.productId)
              }
              wx.navigateBack()
            }
          })
        } else if (res.code == 500) {
          wx.showToast({
            title: res.msg,
            icon: 'none'
          })
        }
      })
    } else if (type == 'delete') {
      deletePicInfoById(productPicInfo.productPicId).then(res => {
        if (res.code == 200) {
          wx.showToast({
            title: '删除成功',
            success: () => {
              const page = getCurrentPages()
              if (page.length > 1) {
                var prePage = page[page.length - 2]
                prePage.searchRs(productPicInfo.productId)
              }
              wx.navigateBack()
            }
          })
        } else if (res.code == 500) {
          wx.showToast({
            title: res.msg,
            icon: 'none'
          })
        }
      })
    }
  },
})