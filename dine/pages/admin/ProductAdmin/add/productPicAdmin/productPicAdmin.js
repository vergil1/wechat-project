// pages/admin/ProductAdmin/add/productPicAdmin/productPicAdmin.js
import {
  getDetailById
} from '../../../../../api/ProductPicInfoApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    productPicList: [],
    maxOrder: '',
    defaultProductId: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.searchRs(options.productId);
    this.setData({
      defaultProductId: options.productId
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  //查询
  searchRs(id) {
    getDetailById(id).then(res => {
      let maxOrder =0;
      console.log(res);
      if (res.data.length>0) {
        maxOrder = res.data[res.data.length - 1].picOrder;
      }
        this.setData({
          productPicList: res.data,
          maxOrder
        })
      
    })
  },
  toAdd() {
    wx.navigateTo({
      url: './pic/pic?maxOrder=' + this.data.maxOrder + "&productId=" + this.data.defaultProductId,
    })
  }

})