// pages/admin/ProductAdmin/add/add.js
import {
  timestampToTime
} from "../../../../utils/util";
import {
  getDetailById,
  addProductInfo,
  modifyProductInfo,
  deleteProductInfo
} from '../../../../api/ProductInfoApi'
import {
  getCategories
} from '../../../../api/ProductCategoryApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    defaultCategoryIndex: 0,
    defaultCategoryName: '',
    cataFlag: false,
    categories: [],
    calanderFlag: false,
    productInfo: {
      productCode: '',
      productName: '',
      descript: '',
      categoryName: '',
      categoryId: '',
      publishStatus: 0,
      hotFlag: 0,
      cover: 0,
      productionDate: '',
      type:1
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    getCategories().then(res => {
      let categories = this.data.categories;
      let productInfo = this.data.productInfo;
      productInfo.categoryId = res.data[0].categoryId;
      for (let index = 0; index < res.data.length; index++) {
        let category = {};
        category.id = res.data[index].categoryId;
        category.name = res.data[index].categoryName;
        categories.push(category)
      }
      this.setData({
        categories,
        productInfo,
        defaultCategoryName: res.data[0].categoryName
      })
    })
    if (options.id) {
      getDetailById(options.id).then(res => {
        console.log(res);
        if (res.code == 200) {
          let productInfo = res.data;
          productInfo.categoryId = res.data.oneCategoryId;
          let defaultCategoryIndex = this.data.defaultCategoryIndex;
          if (productInfo.productionDate) {
            productInfo.productionDate = timestampToTime(productInfo.productionDate, 0)
          }
          for (let index = 0; index < this.data.categories.length; index++) {
            if (this.data.categories[index].categoryId == res.data.categoryId) {
              defaultCategoryIndex = index;
            }
          }
          this.setData({
            productInfo,
            defaultCategoryIndex
          })
        } else {
          wx.showToast({
            title: res.msg,
            icon: 'none'
          })
        }
      })
    }


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },

  //生产日期日历框打开
  productionDateClick() {
    this.setData({
      calanderFlag: true
    })
  },
  //生产日期日历框取消
  onCalanderClose() {
    this.setData({
      calanderFlag: false
    })
  },
  //生产日期日历框确认
  onCalanderConfirm(e) {
    let date = e.detail;
    let productInfo = this.data.productInfo;
    productInfo.productionDate = timestampToTime(date.getTime(), 0)
    this.setData({
      calanderFlag: false,
      productInfo
    })
  },
  //分类下拉框点击弹出
  productionCatalogClick() {
    this.setData({
      cataFlag: true
    })
  },
  //分类下拉框确认
  onCataConfirm(e) {
    let productInfo = this.data.productInfo;
    productInfo.categoryId = e.detail.value.id;
    productInfo.categoryName = e.detail.value.name;
    this.setData({
      productInfo,
      cataFlag: false,
    })
    console.log(this.data.productInfo);
  },
  //分类下拉框取消
  onCataCancel() {
    this.setData({
      cataFlag: false
    })
  },
  //输入框变化
  inputChange(e) {
    let type = e.currentTarget.dataset.type;
    let productInfo = this.data.productInfo;
    if (type == 'code') {
      let code = e.detail;
      productInfo.productCode = code;
    } else if (type == 'name') {
      let name = e.detail;
      productInfo.productName = name;
    } else if (type == 'descript') {
      let descript = e.detail;
      productInfo.descript = descript;
    } else if (type == 'price') {
      let price = e.detail;
      productInfo.price = price;
    } else if (type == 'fee') {
      let fee = e.detail;
      productInfo.fee = fee;
    }else if (type == 'status') {
      let publishStatus= e.detail;
      productInfo.publishStatus = publishStatus;
    }else if (type == 'hot') {
      let hotFlag = e.detail;
      productInfo.hotFlag = hotFlag;
    }else if (type == 'cover') {
      let cover = e.detail;
      productInfo.cover = cover;
    }
    else if (type == 'type') {
      let type = e.detail;
      productInfo.type = type;
    }
    this.setData({
      productInfo
    })
    console.log(this.data.productInfo);
  },
  //添加商品-修改商品-删除商品-图片管理-绑定sku
  submit(e) {
    let type = e.detail.target.dataset.type;
    let productInfo = this.data.productInfo;
    productInfo.oneCategoryId = productInfo.categoryId;
    console.log(productInfo);
    if (type == 'add') {
      if (!productInfo.productCode) {
        wx.showToast({
          title: '商品编码不能为空',
          icon: 'none'
        });
        return;
      }
      if (!productInfo.productCode) {
        wx.showToast({
          title: '商品名称不能为空',
          icon: 'none'
        });
        return;
      }
      addProductInfo(productInfo).then(res => {
        if (res.code == 200) {
          wx.showToast({
            title: '新增成功',
            success: () => {
              const page = getCurrentPages()
              if (page.length > 1) {
                var prePage = page[page.length - 2]
                prePage.search()
              }
              wx.navigateBack()
            }
          })
        } else if (res.code == 500) {
          wx.showToast({
            title: res.msg,
            icon: 'none'
          })
        }
      })
    } else if (type == 'modify') {
      if (!productInfo.productCode) {
        wx.showToast({
          title: '商品编码不能为空',
          icon: 'none'
        });
        return;
      }
      if (!productInfo.productCode) {
        wx.showToast({
          title: '商品名称不能为空',
          icon: 'none'
        });
        return;
      }
      modifyProductInfo(productInfo).then(res => {
        if (res.code == 200) {
          wx.showToast({
            title: '修改成功',
            success: () => {
              const page = getCurrentPages()
              if (page.length > 1) {
                var prePage = page[page.length - 2]
                prePage.search()
              }
              wx.navigateBack()
            }
          })
        } else if (res.code == 500) {
          wx.showToast({
            title: res.msg,
            icon: 'none'
          })
        }
      })
    } else if (type == 'delete') {
      wx.showModal({
        title: '确认删除？',
        success (res) {
          if (res.confirm) {
            deleteProductInfo(productInfo.productId).then(res=>{
              if (res.code == 200) {
                wx.showToast({
                  title: '删除成功',
                  success: () => {
                    const page = getCurrentPages()
                    if (page.length > 1) {
                      var prePage = page[page.length - 2]
                      prePage.search()
                    }
                    wx.navigateBack()
                  }
                })
              } else if (res.code == 500) {
                wx.showToast({
                  title: res.msg,
                  icon: 'none'
                })
              }
            })
          } 
        }
      })
    } else if (type == 'addImg') {
      wx.navigateTo({
        url: '/pages/admin/ProductAdmin/add/productPicAdmin/productPicAdmin?productId='+productInfo.productId,
      })
    } else if (type == 'bindSku') {
      wx.navigateTo({
        url: '/pages/admin/ProductAdmin/add/bindSku/bindSku?productId='+productInfo.productId,
      })
    }
  },true(){}
})

