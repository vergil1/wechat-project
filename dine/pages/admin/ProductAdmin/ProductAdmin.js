// pages/admin/ProductAdmin/ProductAdmin.js
import {
  getCategories
} from '@api/ProductCategoryApi'
import{
  getPageList
} from '@api/ProductInfoApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    value2: "asc", //默认值
    value1: '', //默认值
    categories: [],//分类框
    option2: [{
      text: "创建日期升序",
      value: "asc",
      icon:'none'
    }, {
      text: "创建日期倒序",
      value: "desc",
      icon:'none'
    }],
    queryParam: {
      entity:{
        categoryId:'',
        sort:'',
        value:''//搜索关键词
      },
      current:1,
      size:99,
      
    },
    productList:[]

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    getCategories().then(res => {
      console.log(res);
      let categories = this.data.categories;
      for (let index = 0; index < res.data.length; index++) {
        if (index == 0) {
          let queryParam = this.data.queryParam;
          queryParam.entity.categoryId = res.data[0].categoryId
          queryParam.entity.sort = this.data.option2[0].value
          this.setData({
            queryParam,
            value1: res.data[0].categoryId
          })
        }
        let category = {
          text: res.data[index].categoryName,
          value: res.data[index].categoryId,
          icon:'none'
        }
        categories.push(category)
      }
      this.setData({
        categories
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  //新增产品
  add() {
    wx.navigateTo({
      url: '/pages/admin/ProductAdmin/add/add',
    })
  },
  //下拉框第一个选项变化
  change1(e) {
    let queryParam = this.data.queryParam;
    queryParam.entity.categoryId = e.detail;
    this.setData({
      queryParam
    })
    getPageList(this.data.queryParam).then(res=>{
      if (res.code==200) {
        this.setData({
          productList:res.data
        })
      }
    })
  },
  //下拉框第二个选项变化
  change2(e) {
    let queryParam = this.data.queryParam;
    queryParam.entity.sort = e.detail;
    this.setData({
      queryParam
    })
    getPageList(this.data.queryParam).then(res=>{
      if (res.code==200) {
        this.setData({
          productList:res.data
        })
      }
    })
  },
  //搜索框确认搜索
  searchConfirm(e){
    let queryParam = this.data.queryParam;
    queryParam.entity.value = e.detail;
    this.setData({
      queryParam
    })
    getPageList(this.data.queryParam).then(res=>{
      if (res.code==200) {
        this.setData({
          productList:res.data
        })
        console.log(this.data.productList);
      }
    })
  },
  search() {
    getPageList(this.data.queryParam).then(res => {
      if (res.code == 200) {
        this.setData({
          productList: res.data
        });
      }
    });
  }
})