import Notify from '@vant/weapp/notify/notify';
import {
  getAdvertisement,
  getPageList,
  addAdvertisement,
  modifyAdvertisement,
  deleteAdvertisement
} from "@api/AdvertisementApi"
import {
  timestampToTime
} from "@utils/util"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    Dialogshow: false,
    checked: false,
    queryParam: {
      current: 1,
      size: 999,
      state: 1,
      startTime: new Date(),
      endTime: new Date(),
      entity: {
        state: 1
      }
    },
    advertisement: [],
    currentAd: {
      title: '',
      text: '',
      state: 0,
    },
    date: null,
    show: false,
    minDate: new Date(2024, 0, 1).getTime(),
    maxDate: new Date(2025, 0, 31).getTime(),
    currentIndex: [],
    beforeClose: {}
  },
  //标题输入框
  titleOnChange(e) {
    let currentAd = this.data.currentAd
    currentAd.title = e.detail;
    this.setData({
      currentAd
    })
  },
  textOnChange(e) {
    let currentAd = this.data.currentAd
    currentAd.text = e.detail;
    this.setData({
      currentAd
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      beforeClose: (action) => new Promise((resolve, reject) => {
        resolve();
        console.log(success);
      }).catch(err => {
        console.log(err);
        reject();
      })
    })
  },
  stateOnChange(e) {
    let queryParam = this.data.queryParam
    queryParam.entity.state = e.detail ? 0 : 1;
    this.setData({
      checked: e.detail,
      queryParam
    });
  },
  //点击搜索框的日历
  onDisplay() {
    this.setData({
      show: true
    });
  },
  onCalenderClose() {
    this.setData({
      show: false
    });
  },
  // //修改页面的日历
  // onDisplay2() {
  //   this.setData({
  //     show2: true
  //   });
  // },
  // onCalenderClose2() {
  //   this.setData({
  //     show2: false
  //   });
  // },
  formatDate(date) {
    date = new Date(date);
    return `${date.getMonth() + 1}/${date.getDate()}`;
  },
  //搜索条件日历框确认
  onCalenderConfirm(event) {
    const [start, end] = event.detail;
    console.log(start, end);
    let queryParam = this.data.queryParam;
    queryParam.entity.startTime = start;
    queryParam.entity.endTime = end;
    this.setData({
      show: false,
      date: `${this.formatDate(start)} - ${this.formatDate(end)}`,
      queryParam,
    });
  },
  //跳转新增
  clickToAdd() {
    this.setData({
      currentAd: {},
      Dialogshow: true
    })
  },
  //跳转修改
  clickToDetail(e) {
    this.setData({
      Dialogshow: true,
      currentAd: e.currentTarget.dataset.item
    })
  },
  //选中复选框
  clickCheckBox(event) {
    this.setData({
      currentIndex: event.detail,
    });
  },
  //复选框防止外层冒泡
  bubble() {
    return;
  },
  //新增修改日历框确认
  onCalenderConfirm2(event) {
    const [start, end] = event.detail;
    let currentAd = this.data.currentAd;
    currentAd.startTime = start;
    currentAd.endTime = end;
    this.setData({
      show2: false,
      currentAd,
      date2: `${this.formatDate(start)} - ${this.formatDate(end)}`,
    });
  },
  //查询当前启用
  async getCurrent() {
    let {
      data
    } = await getAdvertisement()
    if (data) {
      data.createTime = timestampToTime(data.createTime, 0)
      let array = [];
      array.push(data)
      this.setData({
        advertisement: array
      })
    }

  },

  addStateOnChange(e) {
    let currentAd = this.data.currentAd;
    currentAd.state = e.detail
    this.setData({
      currentAd
    });
  },
  //删除
  clickToDelete() {
    wx.showModal({
      title: '',
      content: '确认删除？',
      complete: (res) => {
        if (res.confirm) {
          let ids = [];
          let i = this.data.currentIndex;
          for (let index = 0; index < i.length; index++) {
            ids.push(this.data.advertisement[index].id);
          }
          deleteAdvertisement(ids).then(res => {
            if (res.code == 200) {
              this.getPageListByParam()
              this.setData({
                currentIndex:[]
              })
            }
          })
        }
      }
    })
  },
  //新增修改确认按钮
  async sure() {
    let currentAd = this.data.currentAd;
    if (!currentAd.title) {
      Notify("标题不能为空")
      return
    }
    if (!currentAd.text) {
      Notify("内容不能为空")
      return
    }
    if (currentAd.state == null || currentAd.state == undefined) {
      Notify("状态不能为空")
      return
    }
    let code1;
    let msg1;
    //更新
    if (currentAd.id) {
      let {
        code,
        msg
      } = await modifyAdvertisement(currentAd);
      code1 = code;
      msg1 = msg;
      //新增
    } else {
      let {
        code,
        msg
      } = await addAdvertisement(currentAd);
      code1 = code;
      msg1 = msg;
    }
    if (code1 == 200) {
      wx.showToast({
          title: msg1
        }),
        this.setData({
          Dialogshow: false
        })
      return true
    } else if (code1 == 500) {
      Notify(msg1)
      return
    }
    this.getPageListByParam()
  },
  //新增修改取消按钮
  onCancel() {
    this.setData({
      Dialogshow: false
    })
    return
  },
  //分页查询
  async getPageListByParam() {
    let {
      data,
      code,
      msg
    } = await getPageList(this.data.queryParam);
    if (code == 200) {
      console.log(data);
      loopSetStartEndTime(data)
      this.setData({
        advertisement: data
      })
    } else if (code == 500) {
      Notify(msg)
    }
  }
})
//展示时间设置
function loopSetStartEndTime(data) {
  for (let index = 0; index < data.length; index++) {
    if (data[index].createTime) {
      data[index].createTime = timestampToTime(data[index].createTime, 0);
    }
  }
}