import {
  getYearReport,
  getMonthReport
} from '../../../../api/StatisticsApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    chartM1: null,
    chartM2: null,
    chartM3: null,
    chart2: null,
    chartU1: null,
    chartU2: null,
    chartU3: null,
    showMonth: false,
    currentDate: new Date().getTime(),
    currentName: '',
    currMonthRegisterUser:0,
    radarImg:'',
    rs1:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    let date = new Date();
    let year = date.getFullYear();
    let month = date.getMonth() + 1; // 月份从 0 开始，所以需要加1
    let yearMonth = year + '年' + month + '月';
    this.setData({
      currentName: yearMonth,
    })
    await this.getDataList(new Date().getFullYear(), new Date().getMonth() + 1);
  },
  chooseMonth() {
    this.handleCanvarToImg(this);
    this.setData({
      showMonth: true
    })
  },
  onInput(event) {
    this.setData({
      currentDate: event.detail,
    });
  },
  // 弹出框确认事件
  async onConfirm(e) {
    let date = new Date(e.detail);
    // 提取日期对应的年份和月份
    let year = date.getFullYear();
    let month = date.getMonth() + 1; // 月份从 0 开始，所以需要加1
    // 将时间戳转换为年月格式
    let yearMonth = year + '年' + month + '月';
    this.setData({
      showMonth: false,
      currentName: yearMonth,
      radarImg:null,
    })
    await this.getDataList(year, month);
    
  },
  // 取消选择年份的弹框
  onCancel() {
    var wxCharts = require('@utils/wxcharts-min.js');
    let chartM1 = new wxCharts({
      canvasId: 'moneyLineCanvas1',
      type: 'line',
      categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
      series: [{
        name: '金额',
        data: this.data.rs1,
        format: function (val) {
          return val.toFixed(2);
        }
      }],
      yAxis: {
        max: 10000,
        min: 0,
        title: '营业金额 (元)',
        format: function (val) {
          return val.toFixed(2);
        },

      },
      width: 370,
      height: 300,
      dataLabel: true
    });
    this.setData({
      showMonth: false,
      radarImg:null,
      chartM1
    })
  },
  async getDataList(year, month) {
    
    let {
      data
    } = await getMonthReport(year, month);
    console.log(data);
    //交易金额
    let map = data.totalMoneySummary;
    let valuesArray = Object.values(map);
    let rs1 = [];
    let rs2 = [];
    let rs3 = [];
    for (let i = 0; i < valuesArray.length; i++) {
      if (i < 10) {
        rs1.push(valuesArray[i])
      } else if (i >= 10 && i < 20) {
        rs2.push(valuesArray[i])
      } else {
        rs3.push(valuesArray[i])
      }
    }
    this.setData({
      rs1,
    })
    //订单状态
    let pie = data.totalStatus;
    //用户流量
    let loginTimesSummaryVos = data.loginTimesSummaryVos;
    let u1 = [];
    let u2 = [];
    for (let i = 0; i < loginTimesSummaryVos.length; i++) {
      if (i < 15) {
        u1.push(loginTimesSummaryVos[i])
      } else {
        u2.push(loginTimesSummaryVos[i])
      }
    }
    //月份新增用户
    this.setData({
      currMonthRegisterUser:data.registerUserTotal==null?0:data.registerUserTotal
    })
    //来源途径
    let userKnowFrom = data.userKnowFrom
    var wxCharts = require('@utils/wxcharts-min.js');
    let chartM1 = new wxCharts({
      canvasId: 'moneyLineCanvas1',
      type: 'line',
      categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
      series: [{
        name: '金额',
        data: rs1,
        format: function (val) {
          return val.toFixed(2);
        }
      }],
      yAxis: {
        max: 10000,
        min: 0,
        title: '营业金额 (元)',
        format: function (val) {
          return val.toFixed(2);
        },

      },
      width: 370,
      height: 300,
      dataLabel: true
    });
    let chartM2 = new wxCharts({
      canvasId: 'moneyLineCanvas2',
      type: 'line',
      categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
      series: [{
        name: '金额',
        data: rs2,
        format: function (val) {
          return val.toFixed(2);
        }
      }],
      yAxis: {
        max: 10000,
        min: 0,
        title: '营业金额 (元)',
        format: function (val) {
          return val.toFixed(2);
        },

      },
      width: 370,
      height: 300,
      dataLabel: true
    });
    let chartM3 = new wxCharts({
      canvasId: 'moneyLineCanvas3',
      type: 'line',
      categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
      series: [{
        name: '金额',
        data: rs3,
        format: function (val) {
          return val.toFixed(2);
        }
      }],
      yAxis: {
        max: 10000,
        min: 0,
        title: '营业金额 (元)',
        format: function (val) {
          return val.toFixed(2);
        },

      },
      width: 370,
      height: 300,
      dataLabel: true
    });
    let chart2 = new wxCharts({
      canvasId: 'orderStatusCanvas',
      type: 'pie',
      series: pie,
      width: 360,
      height: 300,
      dataLabel: true
    });
    let chartU1 = new wxCharts({
      canvasId: 'loginUserCanvas1',
      type: 'column',
      categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'],
      series: [{
        name: '登录次数',
        data: u1
      }],
      yAxis: {
        max: 500,
        min: 0,
        format: function (val) {
          return val + '次';
        }
      },
      width: 370,
      height: 300
    });
    let chartU2 = new wxCharts({
      canvasId: 'loginUserCanvas2',
      type: 'column',
      categories: ['16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
      series: [{
        name: '登录次数',
        data: u2
      }],
      yAxis: {
        max: 500,
        min: 0,
        format: function (val) {
          return val + '次';
        }
      },
      width: 370,
      height: 300
    });
    let chartU3 = new wxCharts({
      canvasId: 'registerUserKnowFromCanvas',
      type: 'pie',
      series: userKnowFrom,
      width: 360,
      height: 300,
      dataLabel: true
    });
    this.setData({
      chartM1: chartM1,
      chartM2: chartM2,
      chartM3: chartM3,
      chart2: chart2,
      chartU1: chartU1,
      chartU2: chartU2,
      chartU3: chartU3
    });
  },
  handleCanvarToImg(that) {
    wx.canvasToTempFilePath({
      x: 0,
      y: 0,
      width: 370,
      height: 500,
      canvasId: 'moneyLineCanvas1',   //对应canvas的id
      success: function(res) {
        that.setData({ 
          radarImg: res.tempFilePath
        });
      }
    });
  },
})