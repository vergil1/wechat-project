import {
  getFavoriteReport
} from '@api/StatisticsApi'
import {getDetailById} from '@api/ProductInfoApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    yearArray: [],
    sortArray: [{
        text: '收藏最多',
        value: 1
      },
      {
        text: '收藏最少',
        value: '2'
      }
    ],
    value1: 2024,
    value2: 1,
    listData: [],
    queryParam: {
      size: 15,
      current: 1,
      entity: {
        sort: 1,
        year: 2024
      }
    },
    total: 0,
    productDialogShow:false,
    productInfo:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let currentYear = new Date().getFullYear();
    let yearArray = this.data.yearArray
    // 构建包含当前年份及未来20年年份的数组
    for (let i = 0; i <= 10; i++) {
      this.data.yearArray.push({
        text: currentYear + i + '年',
        value: currentYear + i
      });
    }
    this.setData({
        yearArray
      }),
      //查询
      this.getPageList(this.data.queryParam)
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    let {
      total,
      listData
    } = this.data
    if (!(total == listData.length)) {
      let queryParam = this.data.queryParam
      queryParam.current += 1;
      this.getPageList(queryParam)
    }
  },
  //分页查询
  async getPageList(queryParam) {
    let {
      data,
      total
    } = await getFavoriteReport(queryParam);
    this.setData({
      listData: this.data.listData.concat(data),
      total,
      queryParam,
    })
  },
  leftChange(e){
    let year = e.detail
    let queryParam = this.data.queryParam
    queryParam.size=15
    queryParam.current=1
    queryParam.entity.year=year
    this.setData({
      listData:[]
    })
    console.log(this.data.listData);
    this.getPageList(queryParam)
  },
  rightChange(e){
    let sort = e.detail
    let queryParam = this.data.queryParam
    queryParam.size=15
    queryParam.current=1
    queryParam.entity.sort=sort
    this.setData({
      listData:[]
    })
    this.getPageList(queryParam)
  },
  async showProduct(e){
    console.log(e);
    let {pid} = e.currentTarget.dataset
    let {data} = await getDetailById(pid)
    this.setData({
      productInfo:data,
      productDialogShow:true
    })
  }
})