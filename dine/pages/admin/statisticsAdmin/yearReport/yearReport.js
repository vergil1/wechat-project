import {
  getYearReport
} from '../../../../api/StatisticsApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    chart: null,
    chart2: null,
    chart3: null,
    chart4: null,
    showYear: false,
    // 选择时间的年份
    columns: [],
    currentDate: new Date().getTime(),
    // 默认当前年份
    currentIndex: new Date().getFullYear() - (new Date().getFullYear() - 25),
    currentName: new Date().getFullYear() + "年",
    radarImg:'',
    valuesArray:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    let that = this;
    // 调用年份的操作
    that.makeYear();
    await this.getDataList(new Date().getFullYear());
  },

  //--------自定义方法----------
  // 选择年份的区域，弹出弹窗框
  chooseYear() {
    this.handleCanvarToImg(this);
    this.setData({
      showYear: true
    })
  },

  // 弹出框确认事件
  async onConfirm(e) {
    let year = e.detail.value
    await this.getDataList(year);
    this.setData({
      radarImg:null,
      showYear: false,
      currentName: year + "年",
    })
  },
  // 取消选择年份的弹框
  onCancel() {
    var wxCharts = require('@utils/wxcharts-min.js');
    let chart = new wxCharts({
      canvasId: 'moneyLineCanvas',
      type: 'line',
      categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
      series: [{
        name: '交易金额',
        data: this.data.valuesArray,
        format: function (val) {
          return val.toFixed(2) + '元';
        }
      }],
      yAxis: {
        title: '成交金额 (万元)',
        format: function (val) {
          return val.toFixed(2);
        },
        min: 0
      },
      width: 370,
      height: 300
    });
    this.setData({
      showYear: false,
      chart,
      radarImg:null
    })
  },
  // 对选择时间进行单独的处理
  makeYear() {
    let year = []
    let currentDate = new Date()
    for (let j = currentDate.getFullYear() - 25; j < currentDate.getFullYear(); j++) {
      year.push(j)
    }
    for (let i = currentDate.getFullYear(); i <= currentDate.getFullYear() + 25; i++) {
      year.push(i)
    }
    this.setData({
      columns: year,
    })
  },
  async getDataList(year) {
    let {
      data
    } = await getYearReport(year);
    //交易金额
    let map = data.totalMoneySummary;
    let valuesArray = Object.values(map);
    this.setData({
      valuesArray
    })
    //订单状态
    let pie = data.totalStatus;
    //用户流量
    let loginTimesSummaryVos = data.loginTimesSummaryVos;
    //新增用户
    let map2 = data.registerUserMap;
    let registerUserArray = Object.values(map2);
    //来源
    let userKnowFrom = data.userKnowFrom
    var wxCharts = require('../../../../utils/wxcharts-min.js');
    let chart = new wxCharts({
      canvasId: 'moneyLineCanvas',
      type: 'line',
      categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
      series: [{
        name: '交易金额',
        data: valuesArray,
        format: function (val) {
          return val.toFixed(2) + '元';
        }
      }],
      yAxis: {
        title: '成交金额 (万元)',
        format: function (val) {
          return val.toFixed(2);
        },
        min: 0
      },
      width: 370,
      height: 300
    });
    let chart2 = new wxCharts({
      canvasId: 'orderStatusCanvas',
      type: 'pie',
      series: pie,
      width: 360,
      height: 300,
      dataLabel: true
    });
    let chart3 = new wxCharts({
      canvasId: 'loginUserCanvas',
      type: 'column',
      categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
      series: [{
        name: '登录次数',
        data: loginTimesSummaryVos
      }],
      yAxis: {
        format: function (val) {
          return val + '次';
        }
      },
      width: 370,
      height: 300
    });
    let chart4 = new wxCharts({
      canvasId: 'registerUserCanvas',
      type: 'line',
      categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
      series: [{
        name: '新增用户',
        data: registerUserArray,
      }],
      yAxis: {
        title: '用户数量',
        min: 0
      },
      width: 370,
      height: 300
    });
    let chart5 = new wxCharts({
      canvasId: 'registerUserKnowFromCanvas',
      type: 'pie',
      series: userKnowFrom,
      width: 360,
      height: 300,
      dataLabel: true
    });
    this.setData({
      chart: chart,
      chart2: chart2,
      chart3: chart3,
      chart4: chart4,
      chart5: chart5,
    });
  },
  true(){},
  handleCanvarToImg(that) {
    wx.canvasToTempFilePath({
      x: 0,
      y: 0,
      width: 370,
      height: 500,
      canvasId: 'moneyLineCanvas',   //对应canvas的id
      success: function(res) {
        that.setData({ 
          radarImg: res.tempFilePath
        });
      }
    });
  },
})