import {
  getDineSaleFoodRank
} from '@api/StatisticsApi'
import {getDetailById} from '@api/ProductInfoApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    listData: [],
    total: 0,
    productDialogShow:false,
    productInfo:{},
    queryParam: {
      size: 15,
      current: 1,
      entity: {
        sort: 1,
        year: 2024
      }
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
      this.getPageList(this.data.queryParam)
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    let {
      total,
      listData
    } = this.data
    if (!(total == listData.length)) {
      let queryParam = this.data.queryParam
      queryParam.current += 1;
      this.getPageList(queryParam)
    }
  },
  //分页查询
  async getPageList(queryParam) {
    let {
      data,
      total
    } = await getDineSaleFoodRank(queryParam);
    this.setData({
      listData: this.data.listData.concat(data),
      total,
      queryParam,
    })
  },
  leftChange(e){
    let year = e.detail
    let queryParam = this.data.queryParam
    queryParam.size=15
    queryParam.current=1
    queryParam.entity.year=year
    this.setData({
      listData:[]
    })
    console.log(this.data.listData);
    this.getPageList(queryParam)
  },
  rightChange(e){
    let sort = e.detail
    let queryParam = this.data.queryParam
    queryParam.size=15
    queryParam.current=1
    queryParam.entity.sort=sort
    this.setData({
      listData:[]
    })
    this.getPageList(queryParam)
  },
  async showProduct(e){
    console.log(e);
    let {pid} = e.currentTarget.dataset
    let {data} = await getDetailById(pid)
    this.setData({
      productInfo:data,
      productDialogShow:true
    })
  }
})