// pages/login/telephoneLogin/telephoneLogin.js
import {validatePhoneNumber,validatePassword} from "../../../utils/util"
import Toast from '@vant/weapp/toast/toast';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    flag: true,
    time: 0,
    telephone:'',
    pwd:'',
    code:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.circle();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  //进入页面循环
  circle(){
    let time =getApp().globalData.time
    if(time<60){
      this.setData({
        flag:false,
        time
      })
      setTimeout(() => {
        this.circle()
      }, 1000);
    }else{
      return
    }
  },
   //获取验证码
   onClick() {
    let time = getApp().globalData.time;
    console.log(time);
    this.setData({
      flag: false,
      time
    })
    time--;
    getApp().globalData.time = time;
    if(time<0){
      getApp().globalData.time = 60;
      this.setData({
        flag: true,
      });
      return;
    }else{
    setTimeout(() => {
      this.onClick()
    }, 1000);
  }
  },
  //输入框改变方法
  inputOnChange(e){
    let type = e.currentTarget.dataset.type;
    let value = e.detail;
    if(type=='telephone'){
      this.setData({
        telephone:value
      })
    }else if(type=='pwd'){
      this.setData({
        pwd:value
      })
    }else if(type=='code'){
      this.setData({
        code:value
      })
    }
  },
  //验证码登录
  verificationMethodLogin(){
    let {telephone,code} = this.data
    let flag = validatePhoneNumber(telephone);
    if(!flag){
      Toast.fail('请输入正确的手机号码');
      return;
    }
    if(code.length !== 6 || !/^\d{6}$/.test(code)){
      Toast.fail('请输入6位数字的验证码');
      return;
    }
    //TODO 后端接口 验证码
   
  },
  //密码方式登录
  passwordMethodLogin(){
    let {telephone,pwd} = this.data
    let flag = validatePhoneNumber(telephone);
    if(!flag){
      Toast.fail('请输入正确的手机号码');
      return;
    }
    //TODO 后端接口 验证码
  },

})