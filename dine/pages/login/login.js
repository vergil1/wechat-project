// pages/login/login.js
import {
  getOpenId,
  getPhoneNumberAndRegister
} from '../../api/LoginApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    authPopShow: false,
    // account:null,
    // password:null,
    // next:false,
    userInfo: {},
    isRead: false,
    serviceFlag: false,
    privacyFlag: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },


  // onConfirm(){
  //   if(this.data.account){
  //   //TODO 查询账号是否存在
  //   this.setData({
  //     next:true
  //   })
  // }else{
  //   wx.showToast({
  //     title: '账号不能为空',
  //     duration: 1500,
  //     mask: true,
  //     icon:'error'
  //   })
  // }
  // },
  // onCheckPwd(){
  //   //验证密码
  // },
  // toRegister(){
  //   wx.navigateTo({
  //     url: '/pages/register/register',
  //   })
  // },
  // // 获取账号输入框的值
  // inputAccount(e) {
  //   this.setData({
  //     account: e.detail.value
  //   })
  // },
  // // 获取密码输入框的值
  // inputPassword(e) {
  //   this.setData({
  //     password: e.detail.value
  //   })
  // },


  //账号密码登录
  toTelephoneLogin() {
    wx.navigateTo({
      url: './telephoneLogin/telephoneLogin',
    })
  },
  //微信一键登录
  autoLogin() {
    wx.login({
      success: async (res) => {
        let code = res.code;
        let {
          data
        } = await getOpenId(code);
        let userInfo = data;
        this.setData({
          userInfo
        })
        //查不出用户
        if (!userInfo || userInfo.userId === null) {
          this.setData({
            authPopShow: true
          });
        } else {
          getApp().globalData.userInfo = userInfo;
          getApp().globalData.access_token = userInfo.newAccessToken
          wx.setStorageSync('access_token', userInfo.newAccessToken);
          wx.showToast({
            title: '登陆成功',
          })
          if (getApp().globalData.type === 2) {
            wx.reLaunch({
              url: '/pages/dine/index/index',
            })
          }else if(getApp().globalData.type === 3){
            wx.reLaunch({
              url: '/pages/room/index/index',
            })
          } else {
            wx.reLaunch({
              url: '/pages/index/index',
            })
          }

        }
      },
    })
  },
  getPhoneReject() {
    this.setData({
      authPopShow: false
    })
  },
  async getPhoneNumber(e) {
    let msg = e.detail.errMsg
    console.log(e.detail.code) // 动态令牌
    console.log(e.detail.errMsg) // 回调信息（成功失败都会返回）
    console.log(e.detail.errno) // 错误码（失败时返回）
    this.setData({
      authPopShow: false
    })
    if (msg === 'getPhoneNumber:ok') {
      let {
        data: userInfo
      } = await getPhoneNumberAndRegister(e.detail.code, this.data.userInfo.openId)
      getApp().globalData.userInfo = userInfo;
      getApp().globalData.access_token = userInfo.newAccessToken
      wx.setStorageSync('access_token', userInfo.newAccessToken);
      wx.showToast({
        title: '登陆成功',
      })
      console.log(getApp().globalData.type);
      if (getApp().globalData.type === 2) {
        wx.reLaunch({
          url: '/pages/dine/index/index',
        })
      }else if(getApp().globalData.type === 3){
        wx.reLaunch({
          url: '/pages/room/index/index',
        })
      } else {
        wx.reLaunch({
          url: '/pages/index/index',
        })
      }
    } else if (msg === 'getPhoneNumber:fail user deny' || msg === 'getPhoneNumber:fail:user deny') {
      wx.showToast({
        title: '登陆失败',
        icon: 'error'
      })
    }
  },
  //是否同意
  readChange(event) {
    this.setData({
      isRead: event.detail,
    });
  },

  /**
   * 服务
   */
  showServiceDialog() {
    this.setData({
      serviceFlag: true
    })
  },
  serviceAgree() {
    this.setData({
      serviceFlag: false
    })
  },

  /**
   * 隐私
   */
  showPrivacyDialog() {
    this.setData({
      privacyFlag: true
    })
  },
  privacyAgree() {
    this.setData({
      privacyFlag: false
    })
  },
})