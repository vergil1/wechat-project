// pages/login/nameAndAvatarSetting/nameAndAvatarSetting.js
import {uploadObject} from '@api/UploadApi'
import * as LoginAPi from '@api/LoginApi'
import Notify from '@vant/weapp/notify/notify';
const defaultAvatarUrl = 'https://mmbiz.qpic.cn/mmbiz/icTdbqWNOwNRna42FI242Lcia07jQodd2FJGIYQfG0LAJGFxM4FbnQP6yfMxBgJ0F3YRqJCJ1aPAK2dQagdusBZg/0'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo:{
      avatarPicUrl:'',
      userName:'',
      userId:'',
      knowFrom:'',
      providerInvitationCode:''
    },
    from:["身边朋友或亲戚推荐","其他途径","无意中发现"],
    fromFlag:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let userInfo = getApp().globalData.userInfo
    console.log(userInfo);
    let thisUserInfo=this.data.userInfo
    thisUserInfo.avatarPicUrl=defaultAvatarUrl
    thisUserInfo.userId=userInfo.userId
    thisUserInfo.userName=userInfo.userName
    this.setData({
      userInfo:thisUserInfo
    })
  },
  //选择头像
  async onChooseAvatar(e) {
    let userInfo = this.data.userInfo;
    let file={};
    file.type = 'avatar'
    file.url=e.detail.avatarUrl;
    let filePath = await uploadObject(file);
    userInfo['avatarPicUrl']=filePath.msg
    this.setData({
      userInfo
    })
  },
  //跳转"我的"页面
  toPersonPage(){
    wx.reLaunch({
      url: '/pages/me/me',
    })
  },
  //名称输入
  bindKeyInput: async function (e) {
    let userInfo = this.data.userInfo;
    userInfo.userName=e.detail
    this.setData({
      userInfo
    })
  },
  //邀请码
  bindCodeInput: async function (e) {
    let userInfo = this.data.userInfo;
    userInfo.providerInvitationCode=e.detail
    this.setData({
      userInfo
    })
  },
  //确定按钮
  async modifyNameAndAvatar(){
    let {userInfo} =this.data
    let {code,msg,data} = await LoginAPi.updateUserInfo(userInfo)
    if (code==200) {
      wx.showToast({
        title: '修改成功',
      })
      getApp().globalData.userInfo=data
      wx.reLaunch({
        url: '/pages/me/me',
      })
    }else{
      Notify(msg);
    }

  },
  //弹出下拉框
  showFrom(){
    this.setData({
      fromFlag:true
    })
  },
  //下拉框确认
  onClickConfirm(e){
    this.setData({
      ['userInfo.knowFrom']:e.detail.value,
      fromFlag:false,
    })
  },
  //下拉框取消
  onCancel(){
    this.setData({
      fromFlag:false
    })
  },
  true(){}
})