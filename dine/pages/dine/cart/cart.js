import Dialog from '@vant/weapp/dialog/dialog';
import * as OrderCartDine_Api from '@api/OrderCartDineApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderCartList:[],
    totalPrice:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let userInfo = getApp().globalData.userInfo
    if (userInfo&&userInfo.userId) {
     this.refresh();
    }
  
  },
  /**
   * 清空
   */
  onClear() {
    Dialog.confirm({
        title: '是否确认清空'
      })
      .then(() => {
        if (this.data.orderCartList.length>0) {
          this.clearOrderCart();
        }else{
          wx.showToast({
            title: '购物袋没有任何商品',
            icon:'none'
          })
        }
        
      })
      .catch(() => {
        console.log("catch");
      });
  },
  //更改数量
  async changeOrderCartAmount(e) {
    let cartId = e.currentTarget.dataset.id
    let productAmount = e.detail
    let obj = {
      cartId,
      productAmount
    }
    let pm = await OrderCartDine_Api.modifySkuAmount(obj)
    if (pm.code == 200) {
     this.refresh();
    }
  },
  //删除购物车商品
  deleteOrderCartByItemId(e) {
    OrderCartDine_Api.deleteOrderCartByItemId()
  },
  /**
   * 清空购物车
   */
  async clearOrderCart(){
    let {code} = await OrderCartDine_Api.clearOrderCart(2);
    if (code == 200) {
      this.refresh();
    }
  },
   //删除购物内容
   async closeSwipe(e) {
    //获取想删除商品的实体
    let {
      cartId
    } = this.data.orderCartList[e.currentTarget.dataset.index]
    let {
      code
    } = await OrderCartDine_Api.deleteOrderCartByItemId(cartId);
    if (code == 200) {
      this.refresh();
    }
  },
  async refresh(){
    let {data} = await OrderCartDine_Api.getOrderCartsByUserId(getApp().globalData.userInfo.userId, 2);
    let totalPrice=0;
    data.forEach(item=>{
      totalPrice+=item.productAmount*item.price;
    })
    console.log(totalPrice);
    this.setData({
      orderCartList: data,
      totalPrice
    })
  },
  toPay(){
    getApp().globalData.type=2
    wx.navigateTo({
      url: '/pages/dine/pay/pay',
    })
  }
})