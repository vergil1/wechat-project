// pages/dine/pay/after/after.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderSn:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      orderSn:options.ordersn
    })
  },
  toDetail(){
    wx.navigateTo({
      url: '/pages/dine/me/detail/detail?ordersn='+this.data.orderSn,
    })
  },
  toHome(){
    wx.navigateBack({
      delta: 10
    })
  }
})