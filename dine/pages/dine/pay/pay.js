import * as OrderCartApi from '@api/OrderCartDineApi'
import * as OrderMasterDineApi from '@api/OrderMasterDineApi'
import createOrder from '@api/WxPay'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    show: false,
    table: 1,
    tabletemp: 0,
    totalPrice: 0,
    totalAmount: 0,
    radio: "2",
    orderCartList: [],
    phone: '',
    remark: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.refresh();
    const tableNo = getApp().globalData.tableNo
    if (tableNo) {
      this.setData({
        table: tableNo
      })
    }
  },


  /**
   * 餐桌弹框
   */
  showPopup() {
    this.setData({
      show: true
    })
  },
  //关闭
  closePopup() {
    this.setData({
      show: false
    })
  },
  //选择框变化
  tableChange(e) {
    this.setData({
      tabletemp: e.detail.value
    })
  },
  //确认
  tableConfirm() {
    this.setData({
      table: this.data.tabletemp,
      show: false
    })
  },
  /**
   * 页面刷新数据
   */
  async refresh() {
    let {
      data
    } = await OrderCartApi.getOrderCartsByUserId(getApp().globalData.userInfo.userId, 2);
    // } = await OrderCartApi.getOrderCartsByUserId(1, 2);
    let totalPrice = 0;
    let totalAmount = 0;
    data.forEach(item => {
      totalPrice += item.productAmount * item.price;
      totalAmount += item.productAmount
    })

    this.setData({
      orderCartList: data,
      totalPrice,
      totalAmount,
    })
  },
  //触发支付
  async pay() {
    const rs = await this.generateDineOrder();
    console.log(rs);
    //预支付
    rs.openId = getApp().globalData.userInfo.openId
    let {
      data
    } = await createOrder(rs);
    console.log(data);
    wx.requestPayment({
      "timeStamp": String(data.timeStamp),
      "nonceStr": data.nonceStr,
      "package": data.prepayId,
      "signType": "RSA",
      "paySign": data.paySign,
      "success": function (res) {
        setTimeout(() => {
          wx.navigateTo({
            url: '/pages/dine/pay/after/after?ordersn=' + rs.orderSn,
          })
        }, 1500);
      },
      "fail": async function (res) {
        console.log(res);
        await OrderMasterDineApi.dineOrderCancel({
          orderSn: rs.orderSn,
          orderCartIds: rs.orderCartIds
        })
      },
      "complete": function (res) {
        console.log(res);
      }
    })
  },
  phoneChange(e) {
    this.setData({
      phone: e.detail
    })
  },
  remarkChange(e) {
    this.setData({
      remark: e.detail
    })
  },
  async generateDineOrder() {
    let {
      orderCartList
    } = this.data
    let dto = {
      phone: this.data.phone,
      remark: this.data.remark,
      table: this.data.table
    };
    let carts = [];
    orderCartList.forEach(item => {
      let a = {
        orderCartId: item.cartId,
        productName: item.productName,
      }
      carts.push(a);
    })
    dto.carts = carts;
    console.log(dto);
    //提交订单
    let {
      code,
      data
    } = await OrderMasterDineApi.dineOrderCommit(dto);
    if (code == 200) {
      return data;
    } else if (code == 500) {
      wx.showToast({
        title: res.msg,
        icon: "none"
      })
    }
  }
})