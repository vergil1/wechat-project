import {
  getCategoryAndProductByDine
} from '@api/ProductCategoryApi'
import * as ProductInfo_API from '@api/ProductInfoApi'
import * as ProductSku_API from '@api/ProductSkuApi'
import * as OrderCartDine_Api from '@api/OrderCartDineApi'
import {
  dictByCondition
} from '@api/DictApi'
import {
  autoLogin
} from '@api/LoginApi'

Page({
  data: {
    vtabs: [{
      title: "热搜推荐",
      item: []
    }],
    activeTab: 0,
    currProduct: {},
    list: [], //checkbox数量
    wantToBuyAmount: 1, //添加数量
    show: false, //登录标志
    currSkuId: '', //当前skuid
    currSkuPrice: 0, //当前sku价格
    total: 0, //购物袋总数
    totalPrice: 0, //合计
    currSku: {}, //当前sku
    radio: 25,
    adPicUrl: "",
    display: false,
    //用于判断是否能提交
    skuSelectedLength: 0, //需要选择多少个规格才能提交
    hasChooseType: new Set(), //存储已经选择规格
    hasChooseTypeSize: 0, //存储已经选择规格数量
    typeValueMap:new Map(),
  },
  async onLoad(options) {
    console.log('解码：', decodeURIComponent(options))
    console.log('直接获取:', options.tableNo)
    if (options.tableNo) {
      getApp().globalData.tableNo = options.tableNo
    }
    // await this.init(options)
  },
  async onShow() {
    const app = getApp();
    let userInfo = app.globalData.userInfo;
    if (userInfo && userInfo.userId) {
      let {
        data
      } = await OrderCartDine_Api.getOrderCartsTotal();
      this.setData({
        total: data.total,
        totalPrice: data.totalPrice * 100
      })
    }
  },
  //左侧改变
  onTabCLick(e) {
    const index = e.detail.index
    console.log('tabClick', index)
  },
  //右侧改变
  onChange(e) {
    const index = e.detail.index
    console.log('change', index)
  },
  //前往购物车
  toCart() {
    wx.navigateTo({
      url: '/pages/dine/cart/cart',
    })
  },
  //选择规格弹框
  async showPop(e) {
    // 从事件对象中获取数据
    const {
      productid,
      proindex,
      categoryindex
    } = e.currentTarget.dataset;

    // 获取图片数据
    const cate = this.data.vtabs[categoryindex];
    await this.getProductAllSkuType(productid);

    this.setData({
      showFlag: true,
      currProduct: cate.productList[proindex],
      currSkuId: '',
      wantToBuyAmount: 1,
      list: [],
      currSku: {}
    })
    //单选框默认不选
    let a = this.data.skuTypeValueList
    a.forEach(item => {
      let detail = item.detail
      detail.forEach(item => {
        item.checked = true
      })
    })
  },
  /**
   * 获取sku
   * @param {} productId 
   */
  async getProductAllSkuType(productId) {
    let {
      data
    } = await ProductInfo_API.getProductAllSkuType(productId);
    this.setData({
      skuTypeValueList: data,
      skuSelectedLength: data.length
    })
  },
  //关闭弹框
  onClose() {
    this.setData({
      showFlag: false,
      skuSelectedLength: 0,
      hasChooseTypeSize: 0,
      typeValueMap:new Map(),
      hasChooseType: new Set()
    })
  },

  async toggle(event) {
    const {
      index
    } = event.currentTarget.dataset;
    console.log(index);
    const checkbox = this.selectComponent(`.checkboxes-${index}`);
    if (this.data.list.length > 0) {
      let str = index.replace("-", ":");
      let {
        data
      } = await ProductSku_API.getProductSkuByTypeValueStr(str, this.data.currProduct.productId)
      console.log(data);
      this.setData({
        currSkuId: data.skuId,
        currSkuPrice: data.price,
      })
    }
  },
  noop() {},
  //检测到checkbox变化
  onChange1(event) {
    console.log(event.detail);
    this.setData({
      list: event.detail,
    });
  },
  //加入购物车/购买 商品的数量
  amountChange(e) {
    this.setData({
      wantToBuyAmount: e.detail
    })
  },
  //检查有无登录
  checkLogin() {
    getApp().globalData.type = 2
    const app = getApp();
    let userInfo = app.globalData.userInfo;
    if (JSON.stringify(userInfo) === "{}" || !userInfo) {
      this.setData({
        show: true
      });
      return false;
    } else {
      return true;
    }
  },
  /**
   * 添加购物车
   */
  async toOrderCart() {
    if (this.checkLogin()) {
      console.log();
      let sku = this.data.currSku;
      let cartEntity = {
        userId: getApp().globalData.userInfo.userId,
        productId: this.data.currProduct.productId,
        skuId: sku.skuId,
        productAmount: this.data.wantToBuyAmount,
        price: sku.price,
        type: 2
      }
      let {
        code,
        msg
      } = await OrderCartDine_Api.addSkuToCart(cartEntity)

      if (code == 200) {
        wx.showToast({
          title: msg,
          icon: 'success'
        })
      } else {
        wx.showToast({
          title: msg,
          icon: 'none'
        })
      }
      let {
        data
      } = await OrderCartDine_Api.getOrderCartsTotal();
      this.setData({
        total: data.total,
        totalPrice: data.totalPrice * 100
      })
      this.setData({
        showFlag: false
      })
    }
  },
  //结算跳转
  onClickButton() {
    if (this.checkLogin()) {
      wx.navigateTo({
        url: '/pages/dine/pay/pay',
      })
    }
  },
  //规格换成单选框 24-07-02
  //radio变化
  async radioChange(e) {
    let typeId = e.currentTarget.dataset.typeid
    let valueId = e.detail.value;
    //判断是否允许提交
    const flag = this.checkCanCommit(typeId,valueId)
    if (flag) {
      const typeValueMap = this.data.typeValueMap;
      console.log(typeValueMap);
      let typeValue = Array.from(typeValueMap).map(([key, value]) => `${key}:${value}`).join(',');
      console.log(typeValue);
      let res = await ProductSku_API.getDineProductSkuByTypeValueStr(typeValue, this.data.currProduct.productId)
      console.log(res);
      this.setData({
        currSku: res.data
      })
    }
  },
  async autoLogin(token) {
    if (token) {
      let {
        data
      } = await autoLogin(token)
      if (data) {
        getApp().globalData.userInfo = data
        getApp().globalData.access_token = token
      }
    }
  },
  //判断已经选择的规格 跟 这个产品拥有的规格数量是否一样 一样才能提交
  checkCanCommit(typeid,valueid) {
    const {hasChooseType,typeValueMap} = this.data;
    hasChooseType.add(typeid)
    typeValueMap.set(typeid,valueid)
    this.setData({
      hasChooseType,
      hasChooseTypeSize: hasChooseType.size,
      typeValueMap
    })
    console.log(typeValueMap);
    return this.data.hasChooseTypeSize == this.data.skuSelectedLength
  },
  async init(event) {
    console.log(event);
    const token = wx.getStorageSync('access_token')
    if (token) {
      this.autoLogin(token)
      let {
        data
      } = await OrderCartDine_Api.getOrderCartsTotal()
      this.setData({
        total: data.total,
        totalPrice: data.totalPrice * 100
      })
    }
    if (getApp().globalData.display) {
      let {
        data: rs
      } = await dictByCondition({
        code: 'dine-index-photo',
        type: 2
      })
      if (rs.length > 0 && rs[0].value != null) {
        this.setData({
          adPicUrl: rs[0].value,
          display: true
        })
      }
    }
    let {
      data
    } = await getCategoryAndProductByDine(event.detail)
    this.setData({
      vtabs: data,
      hasChooseType: new Set(),
      typeValueMap: new Map(),
    })
  }
  
})

