import {
  detailByOrderSn
} from '@api/OrderMasterDineApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    rs: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    let orderSn = options.ordersn
    let {
      data: rs
    } = await detailByOrderSn(orderSn);
    this.setData({
      rs
    })
  },
})