import * as orderMasterDineApi from '@api/OrderMasterDineApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderList: [],
    userInfo: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(getApp().globalData.userInfo);
    if (getApp().globalData.userInfo) {
      this.setData({
        userInfo: getApp().globalData.userInfo
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  async onShow() {
    if (getApp().globalData.userInfo&&getApp().globalData.userInfo.userId !=null) {
      let queryParam = {
        current: 1,
        size: 999,
        entity: {
          userId: getApp().globalData.userInfo.userId
        }
      }
      let {
        data
      } = await orderMasterDineApi.getPageList(queryParam)
      this.setData({
        orderList: data
      })
    }

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  toShop() {
    wx.switchTab({
      url: '/pages/index/index',
    })
  },
  toBill() {
    wx.navigateTo({
      url: '/pages/dine/me/bill/bill',
    })
  },
  toLogin() {
    getApp().globalData.type=2
    wx.navigateTo({
      url: '/pages/login/login',
    })
  },
  toSet(){
    wx.navigateTo({
      url: '/pages/admin/admin',
    })
  },
  toRank(){
    wx.navigateTo({
      url: '/pages/admin/dineSaleStatistics/dineSaleStatistics',
    })
  },
  toMonthReport(){
    wx.navigateTo({
      url: '/pages/admin/dineMonthReport/dineMonthReport',
    })
  },
  toChef(){
    wx.navigateTo({
      url: '/pages/dine/me/chef/chef',
    })
  },
  logout(){
    getApp().globalData.userInfo={},
    wx.clearStorageSync("access_token")
    wx.reLaunch({
      url: '/pages/dine/me/me',
    })
  },
})