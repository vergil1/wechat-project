import * as dineApi from '@api/OrderMasterDineDetailApi'
Page({
  data: {
    active: 0,
    rs:[],
  },
  onLoad(options) {
    this.getData(0);
  },
  onChange(event) {
    this.getData(event.detail.name);
  },
  async getData(state){
    const {data} = await dineApi.getDishByChef(state);
    this.setData({
      rs:data
    })
  },
  async ready(e){
    const orderDetailId = e.target.dataset.id;
    await dineApi.ready(orderDetailId)
    this.getData(0);
  }
})