import * as orderApi from '@api/OrderMasterDineApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    todayBillVo:{
      totalPrice:0
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    this.getData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  toHistory(){
    wx.navigateTo({
      url: '/pages/dine/me/bill/history/history',
    })
  },
  async getData(){
    let {data} = await orderApi.queryTotalBIll()
    console.log(data);
    this.setData({
      todayBillVo:data
    })
  }
})