import * as util from '@utils/util'
import * as orderApi from '@api/OrderMasterDineApi'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isMonth: false,
    timeText: "",
    monthShow: false,
    dateShow: false,
    currentDate: new Date().getTime(),
    queryParam: {
      current: 1,
      size: 10,
      entity: {
        //1-年月 2-年月日
        type: 2,
        date: new Date().getTime()
      }
    },
    total: 0,
    billVo: {
      detail:[]
    }
  },
  async onChange({
    detail: isMonth
  }) {
    let date = new Date(this.data.timeText)
    let timeText = util.formatDate(date, isMonth === true ? 2 : 1);
    //查询
    await this.getPageList({
      current: 1,
      size: 10,
      entity: {
        //1-年月 2-年月日
        type: isMonth === true ? 1 : 2,
        date: date
      }
    });
    this.setData({
      isMonth,
      timeText
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    let date = new Date();
    let timeText = util.formatDate(date);
    await this.getPageList(this.data.queryParam);
    this.setData({
      timeText
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    // if (this.data.total != this.data.billVo.length) {
    //   let queryParam = this.data.queryParam;
    //   queryParam.current += 1
    //   this.getPageList(queryParam)
    // }
  },
  //  日期选择/月
  monthSelect() {
    this.setData({
      monthShow: true
    })
  },
  //  日期选择/日
  dateSelect() {
    this.setData({
      dateShow: true
    })
  },
  //月份确认
  async monthConfirm(e) {
    let date = new Date(e.detail)
    let timeText = util.formatDate(new Date(date), 2);
    //查询
    await this.getPageList({
      current: 1,
      size: 10,
      entity: {
        //1-年月 2-年月日
        type: 1,
        date: date
      }
    });
    this.setData({
      monthShow: false,
      timeText
    })
  },
  monthCancel() {
    this.setData({
      monthShow: false
    })
  },
  //年月日确认
  async dateConfirm(e) {
    let date = new Date(e.detail)
    let timeText = util.formatDate(date, 1);
    //查询
    await this.getPageList({
      current: 1,
      size: 10,
      entity: {
        //1-年月 2-年月日
        type: 2,
        date: date
      }
    });
    this.setData({
      dateShow: false,
      timeText
    })
  },
  dateCancel() {
    this.setData({
      dateShow: false
    })
  },
  //查询数据
  async getPageList(param) {
    let {
      data,
      total
    } = await orderApi.queryBillByDate(param);
    this.setData({
      billVo:data,
      total
    })
  }
})